using Revise
using CUDA, Zygote, Flux, FFTW
using collatingTransform
using Test
using LinearAlgebra
init = randn(Float32,100,1,1);
inputSize = size(init)
st = scatteringTransform(inputSize,2,useGpu=false, dType=Float32)

using Flux:mse
@time dummyRes = st(init);
[size(x) for x in dummyRes]
typeof(dummyRes)
exampleToFit = ([zeros(Float32, size(x)) for x in dummyRes]...,);
exampleToFit[1][25,1,1] = 1
objectiveFunction(x,y) = sum(mse(a[1],a[2]) for a in zip(st(x),y))
@time objectiveFunction(init,exampleToFit)
typeof(objectiveFunction(init,exampleToFit))

N = 1000
exampleToFit = st(init);
pathOfDescent = zeros(Float32, 100,1,N);
pathOfDescent[:,:,1] = randn(Float32, inputSize);
(typeof(pathOfDescent), size(pathOfDescent))
∇ = gradient((x)->objectiveFunction(x,exampleToFit), pathOfDescent[:,:,1:1])
∇
μ = .001
err = zeros(Float32, N); err[1] = objectiveFunction(pathOfDescent[:,:,1:1],exampleToFit)
for ii=2:N
    err[ii], back = pullback((x)->objectiveFunction(x,exampleToFit), pathOfDescent[:,:,(ii-1):(ii-1)])
    if ii %10==0
        println("$ii $(err[ii])")
    end
    ∇ = back(err[ii])
    pathOfDescent[:,:,ii] = pathOfDescent[:, :, ii-1] - μ .* ∇[1]
end
# ok, so it looks like simple gradient descent won't work
using Plots
heatmap(pathOfDescent[:,1,1:10:1000])
plot(err)
# the zero gradient is correct
∇ = gradient((x)->objectiveFunction(x,exampleToFit), zeros(Float32, 100,1,1))


target = st(init);
currentGuess = init;#randn(Float32,100,1,1);
∇ = gradient((x)->objectiveFunction(x,target), currentGuess); maximum(∇[1]) # this should be zero!

exampleToFit
length(st.mainChain)
y,back = pullback(x->st.mainChain[7](st.mainChain[6](st.mainChain[5](st.mainChain[4](st.mainChain[3](st.mainChain[2](st.mainChain[1](x))))))), init);
typeof(y)
typeof(back(y)[1])
y,back = pullback(x->st.mainChain[4](st.mainChain[3](st.mainChain[2](st.mainChain[1](x)))), init);

obj(x,y) = norm(x-y)
y, back = pullback(x->obj(x,randn(Float32, 10)),randn(Float32,10))
typeof(y)
typeof(back(y))
y, back = pullback(norm, randn(Float32, 10))
y, back = pullback(norm, 5.0)
y
back(y)
typeof(y)
typeof(back(y))
typeof(rees)
typeof(norm(randn(Float32, 10)))
∇ = gradient(norm, randn(Float32, 10))
∇[1]

y,back = pullback((x)->st(x), pathOfDescent[1]);
typeof(back(y))

y,back = pullback(x->(x|>st.mainChain[1] |> st.mainChain[2] |> st.mainChain[3] |> st.mainChain[4] |> st.mainChain[5] |> st.mainChain[6] |> st.mainChain[7]), init); typeof(y)
typeof(collatingTransform.normalize(y,2))
size(back(y)[1])

# fixing apply1D
st = scatteringTransform(inputSize,2,useGpu=false, dType=Float32)
y, back = pullback(collatingTransform.apply1D, st.mainChain[3], init|>st.mainChain[1] |> st.mainChain[2])
backup = back(y);
typeof(backup)
size(init|>st.mainChain[1] |> st.mainChain[2])
size(backup[2])

# fill should be getting something it can sum over, since every previous entry depends on it
function example(x,N)
    ax = axes(x)
    extraAxe = ax[2+N:end]
    filledLoc = fill(1, N)
    return x[:, filledLoc..., extraAxe...]
end
y, back = pullback(example, randn(5,3,4,3), 2)
back(y)


res, back = pullback(fill, 5, 3)
otherGrad = randn(rng,3)
@test back(otherGrad)[1] ≈sum(otherGrad)
# and the case that's actually happened
@test back((nothing,)) == (nothing,nothing)
tmp = (nothing,)
tmp = (nothing, nothing, 3)
tmp[tmp.!=nothing]
tmp[(tmp.!=nothing)...]
back6((nothing,))
fieldnames(typeof(back.back))
code_typed(getfield(back.back, Symbol("#2404#_back")).back6.back, (typeof(y),))
t = getfield(back.back, Symbol("#2404#_back")).back6.back.t
t
back(y)
code_typed(back.back, (typeof(y),))
for x in back.back.t 
    println(typeof(x))
end
typeof(init)
typeof(st.mainChain[1])
typeof(st.mainChain[1].weight)
typeof(st.mainChain[1](init))
typeof(st.mainChain[1])
typeof(y)
back(y)
# add a test for type consistency

# testing input gradients of various things
using Debugger
using Zygote, FourierFilterFlux, LinearAlgebra, FFTW
using FourierFilterFlux:applyWeight, applyBC, internalConvFFT
using Zygote:@showgrad
using Test
thing = waveletLayer(size(init),useGpu=false)
thing.weight
size(thing.weight)
thing.analytic
shears.analytic
xbc, usedInds = applyBC(init,thing.bc,1)
x̂ = thing.fftPlan[1]*xbc
size(x̂)
nextLayer = internalConvFFT(x̂, thing.weight, usedInds, thing.fftPlan[2],
                            nothing, thing.analytic);
∇ = gradient((x̂) -> abs(internalConvFFT(x̂, thing.weight, usedInds, thing.fftPlan[2],
                            nothing, thing.analytic)[1,1,1,1,1]),
             x̂)
y,back = Zygote.pullback(x̂->internalConvFFT(x̂, thing.weight, usedInds, thing.fftPlan[2],
                            nothing, thing.analytic),x̂)
size(y)
res= back(y);
code_typed(back, (typeof(y),)) # this returns a tuple of length 1, which is right
code_typed(back.back, (typeof(y),))
back.back.t[6].t[5]
code_typed(back.back.t[6], (typeof(y),)) # this is ∂(internalConvFFT)
back.back.t[6](y)


# 
gradient(x̂) do x̂
    axShear = axes(shears)
    axx = axes(x̂)[N:end-1]
    if typeof(An) <: Tuple
        isAnalytic = [((ii in An) ? 1 : true) for ii=1:size(shears)[end]]
    else
        isAnalytic = [nothing for ii=1:size(shears)[end]]
    end
    gatheredIters = zip(eachslice(shears,dims=ndims(shears)), isAnalytic)
    λ(iters) = applyWeight(x̂,iters[1],usedInds, fftPlan, (N,N), bias, iters[2])
    #mapped = map(λ, gatheredIters)
    mapped = [applyWeight(x̂, shears[axShear[1:end-1]..., ii], usedInds,
                          fftPlan, (N,ii), bias, isAnalytic[ii]) for ii in
              1:length(gatheredIters)]
    cats = cat(mapped...,dims=1)
    dogs = permutedims(cats, ((2:N)..., 1, (N+1):ndims(mapped[1])...))
    return dogs
end

gradient(2,3) do b
    tmp = @showgrad(a)^2
    return tmp*b
end
# 
shears = thing.weight; fftPlan = thing.fftPlan[2]; An = thing.analytic; bias = nothing; N = ndims(shears)
axShear = axes(shears); axx = axes(x̂)[N:end-1]

isAnalytic = [((ii in An) ? 1 : true) for ii=1:size(shears)[end]]
isAnalytic = trues(14)
λ(enAn) = applyWeight(x̂, shears[axShear[1:end-1]..., enAn[1]], usedInds,
                         fftPlan, (N, enAn[1]), bias, enAn[2])
[x for x in zip(1:4,[4,5,6,7])]
gatheredIters = zip(eachslice(shears,dims=ndims(shears)), isAnalytic)#, 1:size(shears,ndims(shears)))
λ(iters) = applyWeight(x̂,iters[1],usedInds, fftPlan, (N,N), bias, iters[2])
map(iters->length(iters), gatheredIters)
map(λ, gatheredIters)
y, back = pullback(x̂->map(λ, gatheredIters), x̂)
λ.(gatheredIters);
y, back = pullback(x̂->λ.(gatheredIters), x̂)
back(y)
y, back = pullback(x̂->[λ(x) for x in gatheredIters], x̂);
back(y)


# what if we make λ take two arguments
λ(shear, isAn) = applyWeight(x̂,shear, usedInds, fftPlan, (N,N), bias, isAn)
res = map(λ, eachslice(shears,dims=ndims(shears)), isAnalytic);
y, back = pullback(x̂->map(λ, eachslice(shears,dims=ndims(shears)), isAnalytic), x̂);
back(y)



code_typed(back.back.t[1], (typeof(y),))
code_typed(back.back.t[1].t[1].back, (typeof(y),))
(y)
y, back = pullback(x̂->map(λ, enumerate(isAnalytic)), x̂);
y, back = pullback(x̂->map((enAn)->applyWeight(x̂, shears[axShear[1:end-1]..., enAn[1]], usedInds,
                         fftPlan, (N, enAn[1]), bias, enAn[2]), enumerate(isAnalytic)), x̂);
back(y)

# the gradient of a single applyWeight is fine...
∇ = gradient(x̂->norm(applyWeight(x̂, shears[axShear[1:end-1]..., 1], usedInds,
                         fftPlan, (N, 1), bias, true)), x̂);
∇ = gradient(sha->norm(applyWeight(x̂, sha, usedInds,
                         fftPlan, (N, 1), bias, true)), shears[axShear[1:end-1]..., 1]);
∇[1]
# checking the pullback of applyWeight for each type
y, back = pullback(x̂->applyWeight(x̂, shears[axShear[1:end-1]..., 1], usedInds,
                                  fftPlan, (N, 1), bias, true), x̂);
typeof(y)
size(back(y)[1])
y, back = pullback(tmp->applyWeight(x̂, tmp, usedInds,
                                  fftPlan, (N, 1), bias, true), 
                   shears[axShear[1:end-1]..., 1]);
y, back = pullback(applyWeight, x̂, shears[axShear[1:end-1]..., 1], usedInds,
                               fftPlan, (N, 1), bias, true);
typeof(y[200])
typeof(y)
∂ = back(y);
length(∂)
(size(∂[1]), size(∂[2]), ∂[3], ∂[4], ∂[5], ∂[6], ∂[7])

y, back = pullback(FourierFilterFlux.argWrapper, x̂, shears[axShear[1:end-1]..., 1], usedInds,
                               fftPlan, (N, 1), bias, true);
res = back(y)
length(res)

y, back = pullback(x̂->map((ii,isAn)-> applyWeight(x̂, shears[axShear[1:end-1]..., ii], usedInds,
                                             fftPlan, (N, ii), bias, isAn),
                          enumerate(isAnalytic)), x̂);
wef = back(y)
@test minimum(abs.(∇[1][:, 1,1]).≈ 2f0/31)


FFTW.rFFTWPlan<:AbstractFFTs.Plan
# full run
tmpRes = thing(init);
obj(x) = norm(thing(x)-tmpRes)
obj(init)

∇ = gradient((x)->obj(x), zeros(100,1,2))

∇[1]-randn(100,1,2)
gradient((x)->sum(real(thing(x))), randn(100,1,2))
y, back = Zygote.pullback(obj, randn(100,1,2))


# walking through the final method
y, back = Zygote.pullback(x->thing(x), randn(100,1,2))
back(y)
fieldnames(typeof(back.back))
fieldnames(typeof(back.back.t[1].t))
code_typed(back,(typeof(y),))
code_typed(back.back,(typeof(y),))
code_typed(back.back.t[1],(typeof(y),))
back.back.t[1].t(y)
back.back.t[1].t[32](y)
back.back(y)

# testing the padding boundary condition
originalSize = (100,1,10)
x = randn(Float32, originalSize)
weightMatrix = randn(Float32, (100+10)>>1 + 1, 1)
padding = (5,)
shears = ConvFFT(weightMatrix, nothing, originalSize, abs,
                 plan=true, boundary = Pad(padding))
shears
tmpRes = shears(x)
obj(x) = norm(shears(x)-tmpRes)
obj(x)
∇ = gradient((x)->obj(x), randn(originalSize))
size(∇[1])
# confirm it's actually just the boundary
weightMatrix = randn(Float32, 101, 1)
shears = ConvFFT(weightMatrix, nothing, originalSize, abs,
                 plan=true, boundary = Sym())
tmpRes = shears(x)
obj(x) = norm(shears(x)-tmpRes)
obj(x)
∇ = gradient((x)->obj(x .+ 1), randn(originalSize))




∇ = gradient(()->obj(init), params(thing))
∇.grads[params(thing).order[1]]
∇ = gradient(obj, randn(100,1,1))
∇.grads

w= Dense(100,5)
tmp = w(randn(100,10))
G = gradient((x)->norm(w(x) .- tmp), randn(100,10))
G[1]
