using CUDA
using CUDAnative
using CUDAdrv
A = cu(randn(20,52*5));
B = cu(randn(98,92, 52, 5, 2));
function collate(A, B::CuArray)
    # permute B to put the summed indices on the inside
    szB = size(B)
    Bp = reshape(B,(szB[1:2]..., prod(szB[3:4]), szB[5:end]...));
    Bp = permutedims(Bp, (3,1,2,4:ndims(Bp)...))
    result = cu(zeros(eltype(B), size(A,1), szB[1:2]..., szB[5:end]...)); size(result)
    
    for ind in eachindex(view(Bp, 1, axes(Bp)[2:end]...))
        result[:, ind] = A *Bp[:, ind]
    end
    return permutedims(result, (2,3,1,4:ndims(result)...))
end
# (:, 98, 92, 100)
# function collateKernel!(A, B, result)
#     space1Ind = (blockIdx().x - 1)*blockDim().x + threadIdx().x
#     space2Ind = (blockIdx().x - 1)threadIdx().x
#     for i = 
# end

CUDAdrv.@profile collate(A,B);
