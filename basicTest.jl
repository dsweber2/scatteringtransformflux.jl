using Revise
using Flux, Wavelets, Plots, FFTW, Zygote


"""

Similar to Conv, but does pointwise multiplication in the Fourier domain. It
assumes both the filter and the signal are real, which allows us to use an rfft
and half the (complex) coefficients we would otherwise need (this is actually
the same number of real coefficients as we would've used at the full length).
"""
struct fftConv{F,A,V,P}
    σ::F
    weight::A
    bias::V
    reflected::Bool
    fftPlan::P
end
function fftConv(w::AbstractArray{T,N}, b::AbstractVector{T}, σ = identity;
                 reflected = false)
    fftConv(σ, w, b, reflected)
end

f(x) = norm(abs.(fft(x)))
f(rand(10))
Zygote.gradient(f, rand(10))
W = rand(2,3); x = rand(3)
Zygote.gradient(W->sum(W*x), W)
x=[-0.353213 -0.789656 -0.270151; -0.95719 -1.27933 0.223982]
gradient((x)->real(fft(ifft(x))[1]),x)


fftConv(k::NTuple{N,Integer}, ch::Pair{<:Integer, <:Integer}, σ=identity; init
        = )
using Zygote
m = Chain(Dense(10, 5, relu), Dense(5, 2))
x = rand(10)
gs = Zygote.gradient(()-> sum(m(x)), Flux.params(m))

a = param(ones(5))
b = param(ones(5))
c = param(ones(5))
T(x) = abs2(c' * ifft(fft(b.*x).*ones(5))) # weights. weights everywhere
grads = Zygote.gradient(() -> T(4*ones(5)), Flux.params(b,c))
T3(x) = abs2.(c' * (fft(b.*x).*a)) # weights. weights everywhere
grads = Zygote.gradient(() -> T3(4*ones(5)), Flux.params(a,b,c))
T3(4*ones(5))
grads[c]
FF(x) = real.(c'*(x .+ (b .* im)))
grads = Zygote.gradient(() -> FF(4*ones(5)), Flux.params(b,c))

Zygote.gradient(c->abs2(c), 1+ 2im)
Tc(c) = Tracker.gradient(T,)
Tb(b) = Tracker.gradient(T,b)
Tb(0)

W = param(2*ones(5)); b = param(3); F(x) = W'*x .+ b
F( 4 .*ones(5) )
grads = Zygote.gradient(() -> F(4*ones(5)), Flux.params(W, b))
grads[b]
grads[W]
W = rand(2,5); b = rand(2); predict(x) = W*x .+ b
function loss(x, y)
    y = predict
end



# example signal
t = 0:2π/100:2π
f = sin.(t) + .5max.(0,t.-π/4)./(t.-π/4 .+.00001)
cwf = cwt(f)
tmp = abs.(cwt(f))
layer2 = zeros(Complex128, 54,54,100)
for i=1:size(tmp,1)
    layer2[i,:,:] = cwt(tmp[i,:])
end
a = ones(size(tmp,1))/size(tmp,1)
tmp2 = reshape(sum(layer2,1),(size(layer2,2), size(layer2,3)))
heatmap(real(tmp2)')
plot(t,f)
heatmap(1:54, t, real(cwf)')
T(x) = cwt(x)
@time fp(x) = Tracker.gradient(T, x)
fp(2)
supertype(typeof(Flux.Tracker.TrackedReal{Float64}))
typeof(Flux.Tracker.TrackedReal{Float64})
fft

supertype(AbstractArray)
abstract type WaveletClass end
abstract type ContinuousWavelet{T} end
abstract type ContinuousWaveletClass <: WaveletClass end

abstract type WaveletBoundary end
# periodic (default)
struct PerBoundary <: WaveletBoundary end
# zero padding
struct ZPBoundary <: WaveletBoundary end
# constant padding
#struct CPBoundary <: WaveletBoundary end
struct NullBoundary <: WaveletBoundary end
const Periodic = PerBoundary()
const DEFAULT_BOUNDARY = PerBoundary()
const padded = ZPBoundary()
const NaivePer = NullBoundary()


struct Morlet <: ContinuousWaveletClass
    σ::Float64 # \sigma is the time/space trade-off. as sigma->0, the spacial resolution increases; below 5, there is a danger of being non-analytic. Default is 5
    κσ::Float64
    cσ::Float64
end
"""
    morl = Morlet(σ::T) where T<: Real

    return the Morlet wavelet with parameter σ, which controls the time-frequency trade-off. As σ goes to zero, all of the information becomes spatial. It is best to choose σ>5, as the wavelets are no longer analytic (try plotting some using the function daughter).
"""
function Morlet(σ::T) where T<:Real
    κσ=exp(-σ^2/2)
    cσ=1./sqrt(1+κσ^2-2*exp(-3*σ^2/4))
    Morlet(σ,κσ,cσ)
end
Morlet() = Morlet(5.0)
class(::Morlet) = "Morlet"; name(::Morlet) = "morl"; vanishingmoments(::Morlet)=0
const morl = Morlet(5.0)


struct CFW{T} <: ContinuousWavelet{T}
    scalingFactor::Float64 # the number of wavelets per octave, ie the scaling is s=2^(j/scalingfactor)
    fourierFactor::Float64
    coi          ::Float64
    α            ::Int64   # the order for a Paul and the number of derivatives for a DOG
    σ            ::Array{Float64} # the morlet wavelet parameters (σ,κσ,cσ). NaN if not morlet.
    name         ::String
    # function CFW{T}(scalingfactor, fourierfactor, coi, daughterfunc, name) where T<: WaveletBoundary
        # new(scalingfactor, fourierfactor, coi, daughterfunc, name)
    # end
end


"""

The constructor for the CFW struct. w is a type of continuous wavelet, scalingFactor is the number of wavelets between the octaves ``2^J`` and ``2^{J+1}`` (defaults to 8, which is most appropriate for music and other audio). The default boundary condition is periodic, which is implemented by appending a flipped version of the vector at the end (to eliminate edge discontinuities). Alternatives are ZPBoundary, which pads with enough zeros to get to the nearest power of 2 (here the results returned by caveats are relevant, see Torrence and Compo '97), and NullBoundary, which assumes the data is inherently periodic.
"""
function CFW(w::WC, scalingfactor::S=8, a::T=DEFAULT_BOUNDARY) where {WC<:WaveletClass, T<:WaveletBoundary, S<:Real}
    if scalingfactor<=0
        error("scaling factor must be positive")
    end
    namee = name(w)[1:3]
    tdef = get(CONT_DEFS, namee, nothing)
    tdef == nothing && error("transform definition not found; you gave $(namee)")
    # do some substitution of model parameters
    if namee=="mor"
        tdef = [eval(parse(replace(tdef[1],"σ",w.σ))), eval(parse(tdef[2])), -1, [w.σ,w.κσ,w.cσ] , name(w)]
    elseif namee[1:3]=="dog" || namee[1:3]=="pau"
        tdef = [eval(parse(replace(tdef[1], "α", order(w)))), eval(parse(replace(tdef[2], "α", order(w)))), order(w), [NaN], name(w)]
    else
        error("I'm not sure how you got here. Apparently the WaveletClass you gave doesn't have a name. Sorry about that")
    end
    return CFW{T}(Float64(scalingfactor), tdef...)
end
name(s::CFW) = s.name
"""
    daughter = Daughter(this::CFW, s::Real, ω::Array{Float64,1})

given a CFW object, return a rescaled version of the mother wavelet, in the fourier domain. ω is the frequency, which is fftshift-ed. s is the scale variable
"""
function Daughter(this::CFW, s::Real, ω::Array{Float64,1})
    if this.name=="morl"
        daughter = this.σ[3]*(π)^(1/4)*(exp.(-(this.σ[1]-ω/s).^2/2)-this.σ[2]*exp.(-1/2*(ω/s).^2))
    elseif this.name[1:3]=="dog"
        daughter = normalize(im^(this.α)*sqrt(gamma((this.α)+1/2))*(ω/s).^(this.α).*exp.(-(ω/s).^2/2))
    elseif this.name[1:4]=="paul"
        daughter = zeros(length(ω))
        daughter[ω.>=0]=(2^this.α)/sqrt((this.α)*gamma(2*(this.α)))*((ω[ω.>=0]/s).^(this.α).*exp.(-(ω[ω.>=0]/s)))
    end
    return daughter
end

const CONT_DEFS = Dict{String,Tuple{String, String, String}}(
"mor" => ("(4*π)/(σ + sqrt(2 + σ.^2))", #FourierFactorFunction-- this will only work for σ≫1. Otherwise, you need a recursive algorithm to derive it.
      "1/sqrt(2)", #COIFunction
      "daughter = cσ*(π)^(1/4)*(exp.(-(σ-ω/s).^2/2)-κσ*exp.(-1/2*(ω/s).^2))", #This is based on the version in Wavelab, that corresponds to the Morlet wavelet on Wikipedia. It satisfies the admissibility condition exactly
      ),
"pau"=> ("4*π/(2*α+1)", #FourierFactorFunction
            "1*sqrt(2)", #COIFunction
            "daughter = ones(length(ω)); daughter[ω.<0]=0; daughter = daughter.*2α/sqrt(α*gamma(2*α)).*(ω/s).^α.*exp.(-(ω/s))", #DaughterFunction
      ),
"dog" => ("2*π*sqrt(2./(2*α+1))", #FourierFactorFunction
      "1/sqrt(2)", #COIFunction
      "daughter = normalize(im^α*sqrt(gamma(α+1/2))*(ω/s).^α.*exp.(-(ω/s).^2/2))",
      )
)

function eltypes(::CFW{T}) where T
    T
end

# function cwt(Y::VectType, c::CFW{W}; J1::S=NaN) where {T<:Real,VectType<:AbstractArray{T}, S<:Real, W<:WaveletBoundary}
"""
wave = cwt(Y::AbstractArray{T}, c::CFW{W}; J1::S=NaN) where {T<:Real, S<:Real, W<:WaveletBoundary}

return the continuous wavelet transform wave, which is (nscales)×(signalLength), of type c of Y. J1 is the total number of scales; default (when J1=NaN, or is negative) is just under the maximum possible number, i.e. the log base 2 of the length of the signal, times the number of wavelets per octave. If you have sampling information, you will need to scale wave by δt^(1/2).
"""
function cwt(Y, c; J1::S=NaN) where {S<:Real}
    T = eltype(Y)
    n1 = length(Y);

    # J1 is the total number of elements
    if isnan(J1) || (J1<0)
        J1=floor(Int64,(log2(n1))*c.scalingFactor);
    end
    #....construct time series to analyze, pad if necessary
    if eltypes(c) == padded
        base2 = round(Int,log(n1)/log(2));   # power of 2 nearest to N
        x = [Y; zeros(2^(base2+1)-n1)];
    elseif eltypes(c) == DEFAULT_BOUNDARY
        x = [Y; flipdim(Y,1)]
    else
        x= Y
    end

    n = length(x);
    #....construct wavenumber array used in transform [Eqn(5)]

    ω = [0:ceil(Int, n/2); -floor(Int,n/2)+1:-1]*2π

    #....compute FFT of the (padded) time series
    x̂ = fft(x);    # [Eqn(3)]

    wave = zeros(Complex{T}, J1+1, n);  # define the wavelet array

    # loop through all scales and compute transform
    for a1 in 0:J1
        daughter = Daughter(c, 2.0^(a1/c.scalingFactor), ω)
        wave[a1+1,:] = ifft(x̂.*daughter)  # wavelet transform[Eqn(4)]
    end
    wave = wave[:,1:n1]  # get rid of padding before returning

    return wave
end
cwt(Y, w::ContinuousWaveletClass; J1::S=NaN) where {S<: Real} = cwt(Y, CFW(w), J1=J1)
caveats(Y, w::ContinuousWaveletClass; J1::S=NaN) where {S<: Real} = caveats(Y, CFW(w), J1=J1)
cwt(Y) = cwt(Y,Morlet())
caveats(Y) = caveats(Y,Morlet())
# cwt(Y::AbstractArray{T}, w::ContinuousWaveletClass; J1::S=NaN) where {T<: Real, S<: Real} = cwt(Y,CFW(w))
# caveats(Y::AbstractArray{T}, w::ContinuousWaveletClass; J1::S=NaN) where {T<: Real, S<: Real} = caveats(Y,CFW(w))
# cwt(Y::AbstractArray{T}) where T<:Real = cwt(Y,Morlet())
# caveats(Y::AbstractArray{T}) where T<:Real = caveats(Y,Morlet())
cwt(f,Morlet())
cwt(f, CFW(Morlet()))
CFW(Morlet())
supertype(typeof(PerBoundary()))
