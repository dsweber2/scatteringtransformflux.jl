using Distributed
using SharedArrays
addprocs(4)
@everywhere begin
using Plots
using Revise
using CUDA, Zygote, Flux, FFTW
using collatingTransform
using Test
using LinearAlgebra
using FileIO, JLD2
using Optim, LineSearches
ifGpu = identity; gpu
pinkNoise(N; α=1) = pinkNoise(Float64, N; α=α)
function pinkNoise(t::DataType, N; α=1) 
    pink = 1 ./ (t(1) .+ (t(0):t(N>>1))).^t(α) # note: not 1:N>>1+1
    phase = exp.((t(2π)*im) .*rand(t,N>>1+1))
    res = irfft(pink .* phase, N)
    res ./norm(res)
end
init = ifGpu(reshape(pinkNoise(Float64, 128), (128,1,1)))

st = scatteringTransform((128,1,1), 2, useGpu=false, dType=Float64,
                         normalize=true, decreasings=[2,3,2],
                         averagingLengths=[1,2,4],σ=x->relu(real(x)))
wave1, wave2, wave3 = getWavelets(st);
initSt = st(init)
N = Int(1e4)
sizePenalty =[.1 1 10]         # possible λ values 
# debugging tests
#m=1; pAx = (1,); loc=2
#$λ = sizePenalty[2]; loc = 5; pAx = (1,26); m=2
duds = load("/home/dsweber/allHail/projects/collatingTransform/examples/scatteringTransform/fitAllPaths/theseWereNotFit.jld2")
listOfDuds = duds["listOfDuds"]

# bool indicating whether that location was fit correctly
function isMaxCoordRight(saveTo, m, pAx, p)
    tmpDict = load(joinpath(saveTo, "fitting.jld2"))
    res = tmpDict["res"]
    sol = chooseLargest(p,res,st).minimizer
    argmax(st(sol)[m]).I[2:end-1]==pAx
end

function innerLoop(pAx, loc, n, newDudsThisLayer, iLoc, ipAx, m)
    legiblePath = mapreduce(x->"$(x)_", *, pAx)
    saveToOld = "/home/dsweber/allHail/projects/collatingTransform/examples/scatteringTransform/fitAllPaths/m$(m)p$(legiblePath)l$(loc)"
    saveTo = "/home/dsweber/allHail/projects/collatingTransform/examples/scatteringTransform/fitAllPaths/m$(m)p$(legiblePath)l$(loc)/try2"
    # check if we've already done this example, or if the old version did it
    p = pathLocs(m,(loc,pAx...))
    # right
    curGood = isfile(joinpath(saveTo, "fitting.jld2")) &&
          isMaxCoordRight(saveTo, m, pAx, p)
    oldIsGood = isMaxCoordRight(saveToOld, m, pAx, p)
    println("have we already done $(pAx), $(loc) well? $(curGood) was the old version good? $(oldIsGood)")

    if  !curGood && !oldIsGood
        mkpath(saveTo)
        runLog = open(joinpath(saveTo,"runRecord.txt"),"w")
        redirect_stdout(runLog)
        justOne = similar(initSt)
        justOne[p] = 1
        obj = makeObjFun(justOne, p, st, true)
        println("_______________________________________________________________________________________________")
        println("starting layer $m, path $legiblePath, loc $loc")
        println("_______________________________________________________________________________________________")
        res, errs =
            maximizeSingleCoordinate(N, init, p, justOne, st;
                                     allowedTime=90*60, obj=obj,
                                     algo=GradientDescent, 
                                     λ = sizePenalty[m+1], tryCatch=true,
                                     lineSearch=HagerZhang(rho=2.0, delta = .01,
                                                           epsilon=1e-9,
                                                           linesearchmax=20));
        
        save(joinpath(saveTo, "fitting.jld2"), "res", res)
        sol = chooseLargest(p,res,st).minimizer
        
        # check if this was done correctly
        μ = argmax(st(sol)[m])
        maxPathRight = (μ==pAx || (typeof(μ)<:CartesianIndex && μ.I[2:end-1] !=pAx))
        println("$(pAx),$(loc) has max coord $(μ)")
        if maximum(diff(sol[:,1,1])) ≈ 0 || !maxPathRight ||
                              saveToOld in listOfDuds
            # if the solution's largest path isn't the target one,
            # or if the solution is constant, we should record that
            newDudsThisLayer[ipAx, iLoc] = true
        else
            newDudsThisLayer[ipAx, iLoc] = false
        end
            
        err = cat(errs..., dims=1) 
        lengths = [length(errI) for errI in errs]
        
        breakPoints = [1; [sum(lengths[1:ii])+1 for ii = 1:length(lengths)-1]]
        plot(err, yscale=:log10, legend=false, xlabel="iteration", 
             title="Loss points are when the target is increased",
             ylabel="error")
        scatter!(breakPoints, err[breakPoints], yscale=:log10)
        savefig(joinpath(saveTo, "errorDescent.pdf"))
            savefig(joinpath(saveTo, "errorDescent.png"))
        plot(plot(sol[:,1,end],legend=false, 
                  title="fitting layer $m, path $legiblePath, loc $loc, err $(round(err[end],digits=2))"),
             plot(abs.(rfft(sol[:,1,end:end], 1)), legend=false, 
                  title="Positive half of the Fourier coefficients"), 
             layout=(2,1))
        savefig(joinpath(saveTo, "ActuallyFitPlot.pdf"))
        savefig(joinpath(saveTo, "ActuallyFitPlot.png"))
        heatmap(st(sol)[1][:,:,1]',title="First Layer fitting layer $m, path $legiblePath, loc $loc, err $(round(err[end],digits=2))", 
                    xlabel="space",ylabel="frequency")
        savefig(joinpath(saveTo, "firstLayerPlot.pdf"))
        savefig(joinpath(saveTo, "firstLayerPlot.png"))
        plotSecondLayer(st(sol), c=cgrad([:blue, :orange], scale=:log),
                        title = "Second Layer fitting layer $m, path $legiblePath, loc $loc, err $(round(err[end],digits=2))")
        savefig(joinpath(saveTo, "secondLayerPlot.pdf"))
        savefig(joinpath(saveTo, "secondLayerPlot.png"))
        redirect_stdout(stderr)
        close(runLog)
    end

    println("just finished $(pAx), $(loc)")
end

"""
    isBelowLine(pAx, nl)
calculate the indices below the diagonal, where layer 2 has a smaller
frequency than layer 1. pAx is a tuple (index layer 1, index layer 2), while nl is the maximum for each
"""
function isBelowLine(pAx, nl)
    diagonal = range(1,stop=nl[1],length=nl[2])
    ceil(diagonal[pAx[2]]) >= pAx[1] 
end
end
newDuds = map(m->SharedArray([false for (i, pAx) in enumerate(Iterators.product(axes(initSt[m])[2:end-1]...)), (j,loc) in enumerate(1:3)]), tuple(0:2...))
println("starting loop")
for m=1:2
    pAxes = Iterators.product(axes(initSt[m])[2:end-1]...)
    n = size(initSt[m])[1]
    spatialLocs = (5, round(Int, n/2), n-5)
    println("__________________________________________________________________")
    println("m=$m, starting distributed")
    println("__________________________________________________________________")
    @sync for (ipAx, pAx) in enumerate(pAxes)
        size(wave1, 2)
        if m < 2 || isBelowLine(pAx, (size(wave2,2), size(wave1,2)))
            println("queue $pAx")
            for (iLoc, loc) in enumerate(spatialLocs)
                @spawnat :any innerLoop(pAx,loc, n, newDuds[m+1], iLoc, ipAx,m)
            end
        else
            println("$(pAx) is above the diagonal, so the 2nd layer frequency is higer than the first layer")
        end
    end
end
save("/home/dsweber/allHail/projects/collatingTransform/examples/scatteringTransform/fitAllPaths/theseWereNotFitRound2.jld2",
     "newDuds", newDuds)
