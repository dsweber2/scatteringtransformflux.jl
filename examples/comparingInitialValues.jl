using Plots
using Revise
using CUDA, Zygote, Flux, FFTW
using collatingTransform
using Test
using LinearAlgebra
using FileIO, JLD2
using Optim
ifGpu = identity; gpu
gauss = randn(Float64, 128,1,1);
flat = ifGpu(ones(Float32,128,1,1));
flatFourier = ifGpu(irfft(exp.(2π*im*rand(Float32,65,1,1)), 128,1)); # start with a uniform
# frequency distribution with random phase
pinkNoise(N; α=1) = pinkNoise(Float64, N; α=α)
function pinkNoise(t::DataType, N; α=1) 
    pink = 1 ./ (t(1) .+ (t(0):t(N>>1))).^t(α) # note: not 1:N>>1+1
    phase = exp.((t(2π)*im) .*rand(t,N>>1+1))
    res = irfft(pink .* phase, N)
    res ./norm(res)
end
pink = reshape(pinkNoise(Float32, 128), (128,1,1))
st = scatteringTransform((128,1,1), 2, useGpu=false, dType=Float64,
                         normalize=true, decreasings=[2,3,2],
                         averagingLengths=[1,2,4],σ=x->relu(real(x)))
initSt = st(init)
sizePenalty = [1e-5 1e-5 1e-10]
λ = sizePenalty[2]; loc = 14; pAx = (5,17); m=2
p = pathLocs(m,(loc,pAx...))
justOne = similar(initSt)
justOne[p] = 1
obj = makeObjFun(justOne, p, st, true)
# make a version that targets the max directly
target = similar(initSt)
target[p] = prod(size(target[m])[2:end-1])
maxObj = makeObjFun(target, p, st, true, 10)

resGauss = maximizeSingleCoordinate(N, gauss, p, justOne,st;
                               allowedTime=60*60, obj=obj, 
                               λ = sizePenalty[m+1],
                               tryCatch=true)
dirGauss,λ = fitUsingOptim(100000000,gauss, p, target, st; obj=maxObj,
                           λ=sizePenalty[m+1], timeAl=60*60) 

InitialHagerZhang()
HagerZhang()
αG = InitialHagerZhang(ψ0=0.01e0,ψ1=.2e0, ψ2=6e0, ψ3=.1e0,
                       αmax=Float64(Inf), α0=Float64(300), quadstep=true, verbose=false)
δ=.1; σ=δ;
gauss
res,errs = fitByPerturbing(N, init, p, justOne,st;
                           allowedTime=60*60, obj=obj,
                           algo=GradientDescent,
                           λ = sizePenalty[m+1], tryCatch=true,
                           lineSearch=HagerZhang(linesearchmax=5000));
dirGauss, λ, err = fitByPerturbing(100000000, gauss, p, target, st;
                                   timeAl=60*60, λ=10, algo=BFGS,
                                   lineSearch = HagerZhang(linesearchmax=5000,
                                                           sigma=σ,
                                                           delta=δ),
                                   alphaguess = αG, NSamples = 1000,
                                   allowIncrease=true);
plotSecondLayer(st(dirGauss.minimizer))
eps(Float64)

resFlat = maximizeSingleCoordinate(N, flat, p, justOne,st;
                               allowedTime=60*60, obj=obj, 
                               λ = sizePenalty[m+1],
                               tryCatch=true)
dirFlat,λ = fitUsingOptim(100000000,flat, p, target, st; obj=maxObj,
                           λ=sizePenalty[m+1], timeAl=60*60) 
st(dirFlat.minimizer)[p]
resFF = maximizeSingleCoordinate(N, flatFourier, p, justOne,st;
                               allowedTime=60*60, obj=obj, 
                               λ = sizePenalty[m+1],
                               tryCatch=true)
dirFF,λ = fitUsingOptim(100000000,flatFourier, p, target, st; obj=maxObj,
                        λ=sizePenalty[m+1], timeAl=60*60) 
st(dirFF.minimizer)[p]
resPink = maximizeSingleCoordinate(N, pink, p, justOne,st;
                               allowedTime=60*60, obj=obj, 
                               λ = sizePenalty[m+1],
                               tryCatch=true)
dirFF,λ = fitUsingOptim(100000000,flatFourier, p, target, st; obj=maxObj,
                        λ=sizePenalty[m+1], timeAl=60*60) 
st(dirFF.minimizer)[p]

 
errGauss = cat([[x.value for x in oneTarget.trace] for oneTarget in resGauss]...,dims=1)
lenGauss = [length(oneTarget.trace) for oneTarget in resGauss]
breakGauss = [1; [sum(lenGauss[1:ii])+1 for ii = 1:length(lenGauss)-1]]
maxGauss = round(maximum([st(x.minimizer)[p][1] for x in resGauss]), digits=1)
plot(errGauss, yscale=:log10, label="Gaussian $(maxGauss)", xlabel="iteration", title="Comparing start locations", legend=:bottomright,
     ylabel="error")
scatter!(breakGauss, errGauss[breakGauss], yscale=:log10, label="")

errFlat = cat([[x.value for x in oneTarget.trace] for oneTarget in resFlat]...,dims=1)
lenFlat = [length(oneTarget.trace) for oneTarget in resFlat]
breakFlat = [1; [sum(lenFlat[1:ii])+1 for ii = 1:length(lenFlat)-1]]
maxFlat = round(maximum([st(x.minimizer)[p][1] for x in resFlat]), digits=1)
plot!(errFlat, yscale=:log10, label="flat $(maxFlat)", xlabel="iteration", 
      ylabel="error")
scatter!(breakFlat, errFlat[breakFlat], yscale=:log10, label="")

errFF = cat([[x.value for x in oneTarget.trace] for oneTarget in resFF]...,dims=1)
lenFF = [length(oneTarget.trace) for oneTarget in resFF]
breakFF = [1; [sum(lenFF[1:ii])+1 for ii = 1:length(lenFF)-1]]
maxFF = round(maximum([st(x.minimizer)[p][1] for x in resFF]), digits=1)
plot!(errFF, yscale=:log10, label="Flat fourier random phase $(maxFF)", xlabel="iteration", 
     ylabel="error")
scatter!(breakFF, errFF[breakFF], yscale=:log10, label="")

errPink = cat([[x.value for x in oneTarget.trace] for oneTarget in resPink]...,dims=1)
lenPink = [length(oneTarget.trace) for oneTarget in resPink]
breakPink = [1; [sum(lenPink[1:ii])+1 for ii = 1:length(lenPink)-1]]
maxPink = round(maximum([st(x.minimizer)[p][1] for x in resPink]), digits=1)
plot!(errPink, yscale=:log10, label="Pink Noise $(maxPink)", xlabel="iteration", 
     ylabel="error")
scatter!(breakPink, errPink[breakPink], yscale=:log10, label="")
savefig("fitAllPaths/comparingStartLocations_m1_j13_10_l21.pdf")
savefig("fitAllPaths/comparingStartLocations_m2_j13_10_l21.png")

savefig("fitAllPaths/comparingStartLocations_m2_j13_10_l21.pdf")
savefig("fitAllPaths/comparingStartLocations_m2_j13_10_l21.png")








# is it actually a problem of saddle points that causes BFGS to stop?
# it states that the objective increases at the point its stepping to
dirGauss.minimizer
function lineEval(loc,fun,stop)
    ∇ = gradient(fun, loc)
    evalAt = 10 .^(range(-20,stop=stop,length=99))
    evalAt = [-reverse(evalAt); 0; evalAt]
    stepping = [fun(loc - μ .* ∇[1]) - fun(loc) for μ in evalAt]
    return evalAt, stepping
end
function lineEval(loc,fun,stop, gradFrom)
    ∇ = gradient(fun, gradFrom)
    evalAt = 10 .^(range(-20,stop=stop,length=99))
    evalAt = [-reverse(evalAt); 0; evalAt]
    stepping = [fun(loc - μ .* ∇[1]) - fun(loc) for μ in evalAt]
    return evalAt, stepping
end
function lineEval(loc,fun,stop, gradFrom, rate)
    ∇ = gradient(fun, gradFrom)
    ∇1 = gradient(fun, loc)
    evalAt = 10 .^(range(-20,stop=stop,length=99))
    evalAt = [-reverse(evalAt); 0; evalAt]
    stepping = [fun(loc - μ .* (rate.*∇[1] .+ ∇1[1])) - fun(loc) for μ in evalAt]
    return evalAt, stepping
end

problemPoint = dirGauss.minimizer
y, back = pullback(maxObj, problemPoint)
plot(problemPoint[:,1,1])
plot(back(y)[1][:,1,1])
plot(lineEval(gauss, maxObj,-2.5)...)
plot(lineEval(dirGauss.minimizer, maxObj,-4)...)
plot(lineEval(dirGauss.minimizer, maxObj,0, gauss, .1)...)
αϕ = lineEval(dirGauss.minimizer, maxObj,0)
plot(αϕ, title=L"f(x-\mu\nabla f(x))", legend=false, xlabel=L"\mu")
∇ = gradient(maxObj, dirGauss.minimizer)[1][:,1,1]

wolfLine = -∇' *∇ /norm(∇) .* αϕ[1][101:end]
plot!(αϕ[1][101:end], wolfLine)
αϕ[1][100 .+ (1:50)]
wolfLine[1:50]
plot(αϕ[1][100 .+ (1:50)], wolfLine[1:50])
function ϕ(α,x₀,fun) 
    ∇ = gradient(fun,x₀)[1]
    return fun(x₀ - α .* ∇)
end
function ∇ϕ(α,x₀,fun)
    ∇ = gradient(fun,x₀)[1][:,1,1]
    return ∇' * gradient(fun, x₀ - α .* ∇)[1][:,1,1]
end
b = .2;
c= -b * ∇ϕ(0,gauss,maxObj)/(∇ϕ(.2,gauss,maxObj) - ∇ϕ(0,gauss,maxObj))
∇ϕ(c,gauss,maxObj)
∇ϕ(b,gauss,maxObj)
ϕ(0,gauss,maxObj)
ϕ(b,gauss,maxObj)
savefig("startingPointGradientLineEval.png")
maxObj(gauss)
norm(gradient(maxObj, gauss))
norm(gradient(maxObj, dirGauss.minimizer))
norm(gauss)
plot(err,scale=:log10)
plot(dirGauss.minimizer[:,1,1])
heatmap(st(dirGauss.minimizer)[1][:,:,1]')
norm(dirGauss.minimizer)
sum(mse(a[1],a[2]) for a in zip(st(dirGauss.minimizer)[p],target[p]))
# so it looks like the problem is with the second order estimation, because the
# gradient could have any step amount
# the gradient is also very tiny compared to that at
dirGauss
dispAllTheThings = sum([2^i for i=0:13])
restmp = fitByPerturbing(N, init, p, justOne, st;
                           timeAl=20*60, allowIncrease = false,
                           algo=GradientDescent,
                           λ = 10, tryCatch=true,
                           lineSearch=HagerZhang(rho=2.0,delta=.01, epsilon=1e-9,
                                                 display=0,
                                                 linesearchmax=20));
res,errs = maximizeSingleCoordinate(N, init, p, justOne,st;
                                allowedTime=60*60, obj = maxObj,
                                algo=GradientDescent, 
                                λ = sizePenalty[m], tryCatch=true,
                                lineSearch=HagerZhang(rho=2.0, delta = .01,
                                epsilon=1e-9, linesearchmax=20));
restmp[1].minimizer

restmp[3]
plot(restmp[1].minimizer[:,1,1])
plot(restmp[3],scale=:log10)
HagerZhang()
justOne[p]
maxObj = makeObjFun(justOne, p, st, true, 1)
res.minimizer
res
res[1].minimum-norm(res[1].minimizer)*λ
gradient(maxObj, restmp[1].minimizer)

∇init = lineEval(restmp[1].minimizer, maxObj, -9)
plot(∇init...)
