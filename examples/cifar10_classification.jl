using Revise
using CUDA, Zygote, Plots, cartoonLike, FFTW, LinearAlgebra, collatingTransform, Flux, LinearAlgebra
using Flux: mse, crossentropy, @epochs, onehotbatch, throttle, onecold
using Base.Iterators: partition
using MLDatasets, Dates
using Plots, Images, Statistics
#using BSON: @save
using JLD2
println("loaded the packages")
train_x, train_y = CIFAR10.traindata()
test_x, test_y = CIFAR10.testdata()
c = collatingLayer((32, 32, 3, 20), 3=>1, σ=abs)
X = cu(randn(32,32,3,20));
size(c(X))
tmpConv = Conv((1,1,1), 3=>16, abs, init=Flux.glorot_uniform)
size(c.layers[1](X))
ch = [49, 49, 16]
batchSize = 20
nClasses = 10
imSizes = zeros(Int32, 3); imSizes[1] = 32
imSizes[2] = poolSize((6//5, 6//5), (imSizes[1], imSizes[1]))[1]
imSizes[3] = poolSize((3//2, 3//2), (imSizes[2], imSizes[2]))[1]
denseSizes = [imSizes[3]^2*ch[end], 3179, 3179, 10]
stack = Chain(collatingLayer((32, 32, 3, batchSize), 3=>ch[1], σ=abs),
              (x) -> maxpool(x, (6//5, 6//5)),
              collatingLayer((26, 26, ch[1], batchSize), ch[1]=>ch[2], σ=abs),
              (x) -> maxpool(x, (3//2, 3//2)),
              collatingLayer((17, 17, ch[2], batchSize), ch[2]=>ch[3], shearLevel=2, σ=abs),
              (x) -> reshape(x, (imSizes[3]^2*ch[end], :)),
              Dense(denseSizes[1:2]..., abs),
              Dense(denseSizes[2:3]..., abs),
              Dense(denseSizes[3:4]..., abs),
              softmax) |> gpu
# stack = Chain(collatingLayer((32, 32, 3, batchSize), 3=>ch[1], σ=abs),
#               (x) -> maxpool(x, (6//5, 6//5)),
#               collatingLayer((26, 26, ch[1], batchSize), ch[1]=>ch[2], σ=abs),
#               collatingLayer((26, 26, ch[2], batchSize), ch[2]=>ch[3], σ=abs),
#               (x) -> maxpool(x, (3//2, 3//2)),
#               collatingLayer((17, 17, ch[3], batchSize), ch[3]=>ch[4], shearLevel=2, σ=abs),
#               (x) -> reshape(x, (imSizes[3]^2*ch[end], :)),
#               Dense(denseSizes[1:2]..., abs),
#               Dense(denseSizes[2:3]..., abs),
#               Dense(denseSizes[3:4]..., abs),
#               softmax) |> gpu
println("built the model")
trainLabels = onehotbatch(train_y, 0:9);
train = gpu.([(train_x[:, :, :, i], trainLabels[:,i]) for i in partition(1:(size(train_x,4)-1000), batchSize)]);
valset = collect(49001:50000)
valX = train_x[:,:,:,valset] |> gpu;
valY = trainLabels[:, valset] |> gpu;
size(train[1][1])
stack(train[1][1])
loess(x, y) = crossentropy(stack(x),y)
accuracy(x, y) = mean(onecold(x, 0:9) .== onecold(y, 0:9))
loss(train[1][1], train[1][2])
gradient(()->loss(train[1][1], train[1][2]), params(stack))
startTime = now()
evalcb = throttle(1*60) do
    acc = accuracy(batchOff(stack, valX, batchSize), valY)
    @show(acc)
    tmp = params(cpu(stack))
    #JLD2.@save "CIFAR10_Checkpoints/$(startTime)/model-checkpoint$(now()).jld2" tmp acc
end
println("loaded the data fully")

opt = ADAM()


JLD2.@load "CIFAR10_Checkpoints/model-checkpoint2019-09-15T13:20:05.957.jld2" tmp

Params(stack) = gpu(tmp)
mkdir("CIFAR10_Checkpoints/$(startTime)")
tmp = params(cpu(stack))

JLD2.@save "CIFAR10_Checkpoints/$(startTime)/start-checkpoint$(now()).jld2" tmp

println("saved the first checkpoint")
@epochs 5 Flux.train!(loess, params(stack), train, opt, cb = evalcb)
(x,y)->crossentropy(stack(x), y)
JLD2.@save "CIFAR10_Checkpoints/$(startTime)/end-checkpoint$(startTime).jld2" tmp

