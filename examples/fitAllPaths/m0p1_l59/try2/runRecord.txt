_______________________________________________________________________________________________
starting layer 0, path 1_, loc 59
_______________________________________________________________________________________________
initial objective function is 1.273679126883981
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 27.0
Iter     Function value   Gradient norm 
     0     1.373679e+00     7.222044e-02
 * time: 5.9604644775390625e-6
perturbing for the 1.0th time. rate = 0.1
0.5931217113898273
Iter     Function value   Gradient norm 
     0     5.931217e-01     2.399397e-02
 * time: 8.821487426757812e-6
----------------------------------------------
The true error, without regularization: 0.5461949593720081
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 26.984219669634733
Iter     Function value   Gradient norm 
     0     5.919486e-01     2.124691e-02
 * time: 1.0967254638671875e-5
perturbing for the 1.0th time. rate = 0.1
0.5895969650972956
Iter     Function value   Gradient norm 
     0     5.895970e-01     1.971612e-02
 * time: 5.9604644775390625e-6
    20     5.809847e-01     2.537073e-02
 * time: 9.484870195388794
----------------------------------------------
The true error, without regularization: 0.5442194159408822
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 26.96755933836246
Iter     Function value   Gradient norm 
     0     5.809847e-01     2.537073e-02
 * time: 9.059906005859375e-6
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.5806996150746572
Iter     Function value   Gradient norm 
     0     5.806996e-01     4.654606e-02
 * time: 9.059906005859375e-6
    20     5.763415e-01     4.139041e-02
 * time: 9.020644903182983
----------------------------------------------
The true error, without regularization: 0.5429885821496888
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 26.962496704855546
Iter     Function value   Gradient norm 
     0     5.763415e-01     4.139041e-02
 * time: 1.0013580322265625e-5
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.5761505770243419
Iter     Function value   Gradient norm 
     0     5.761506e-01     3.602934e-02
 * time: 5.0067901611328125e-6
----------------------------------------------
The true error, without regularization: 0.5418204735299761
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 26.94165083903971
Iter     Function value   Gradient norm 
     0     5.747705e-01     2.818180e-02
 * time: 1.1205673217773438e-5
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.5741828490601528
Iter     Function value   Gradient norm 
     0     5.741828e-01     5.043196e-02
 * time: 9.059906005859375e-6
----------------------------------------------
The true error, without regularization: 0.540654328949576
----------------------------------------------
perturbing for the 2.0th time. rate = 0.080000006
couldn't find a better location. Adjust stepSize or number of samples K
0.5756416238466109
Iter     Function value   Gradient norm 
     0     5.756416e-01     6.904989e-02
 * time: 7.867813110351562e-6
----------------------------------------------
The true error, without regularization: 0.5410661625457859
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 26.378581547693063
Iter     Function value   Gradient norm 
     0     5.749525e-01     3.942441e-02
 * time: 1.1205673217773438e-5
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.575579650459295
Iter     Function value   Gradient norm 
     0     5.755797e-01     3.812165e-02
 * time: 7.152557373046875e-6
----------------------------------------------
The true error, without regularization: 0.5403972761082401
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 26.34370423469692
Iter     Function value   Gradient norm 
     0     5.750106e-01     3.599954e-02
 * time: 1.1205673217773438e-5
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.5766228062867526
Iter     Function value   Gradient norm 
     0     5.766228e-01     5.624317e-02
 * time: 9.059906005859375e-6
----------------------------------------------
The true error, without regularization: 0.5387681773522405
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 26.191693562990874
Iter     Function value   Gradient norm 
     0     5.743401e-01     3.839025e-02
 * time: 8.106231689453125e-6
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.5770289990235493
Iter     Function value   Gradient norm 
     0     5.770290e-01     1.099457e-01
 * time: 6.9141387939453125e-6
----------------------------------------------
The true error, without regularization: 0.5383414762716059
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 26.164466475263165
Iter     Function value   Gradient norm 
     0     5.753859e-01     5.848689e-02
 * time: 1.2874603271484375e-5
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.578354824422314
Iter     Function value   Gradient norm 
     0     5.783548e-01     1.191219e-01
 * time: 6.9141387939453125e-6
----------------------------------------------
The true error, without regularization: 0.5370893307734704
----------------------------------------------
perturbing for the 2.0th time. rate = 0.080000006
couldn't find a better location. Adjust stepSize or number of samples K
0.5791017409327417
Iter     Function value   Gradient norm 
     0     5.791017e-01     1.211232e-01
 * time: 5.9604644775390625e-6
----------------------------------------------
The true error, without regularization: 0.537624981465055
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 25.552262666950995
Iter     Function value   Gradient norm 
     0     5.779235e-01     5.528238e-02
 * time: 9.059906005859375e-6
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.5819506033700862
Iter     Function value   Gradient norm 
     0     5.819506e-01     9.743102e-02
 * time: 6.9141387939453125e-6
----------------------------------------------
The true error, without regularization: 0.5374179232011239
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 25.328588895133244
Iter     Function value   Gradient norm 
     0     5.801262e-01     4.412173e-02
 * time: 9.059906005859375e-6
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.5839710244183279
Iter     Function value   Gradient norm 
     0     5.839710e-01     1.591997e-01
 * time: 5.9604644775390625e-6
----------------------------------------------
The true error, without regularization: 0.5369153220762656
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 25.164762936461372
Iter     Function value   Gradient norm 
     0     5.815414e-01     1.023191e-01
 * time: 8.106231689453125e-6
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.5844096107610527
Iter     Function value   Gradient norm 
     0     5.844096e-01     1.476813e-01
 * time: 5.9604644775390625e-6
----------------------------------------------
The true error, without regularization: 0.534620250539698
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 24.754937489002096
Iter     Function value   Gradient norm 
     0     5.820548e-01     9.452515e-02
 * time: 5.0067901611328125e-6
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.587104409192668
Iter     Function value   Gradient norm 
     0     5.871044e-01     1.947369e-01
 * time: 8.106231689453125e-6
----------------------------------------------
The true error, without regularization: 0.5332308842739522
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 24.75347730855422
Iter     Function value   Gradient norm 
     0     5.834061e-01     6.989890e-02
 * time: 1.0967254638671875e-5
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.5915462064724772
Iter     Function value   Gradient norm 
     0     5.915462e-01     2.053486e-01
 * time: 5.9604644775390625e-6
----------------------------------------------
The true error, without regularization: 0.5337098709370941
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 24.660234892372586
Iter     Function value   Gradient norm 
     0     5.869791e-01     6.597789e-02
 * time: 9.775161743164062e-6
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.5946776692470787
Iter     Function value   Gradient norm 
     0     5.946777e-01     2.389315e-01
 * time: 8.106231689453125e-6
----------------------------------------------
The true error, without regularization: 0.5338888920777787
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 24.30713304856649
Iter     Function value   Gradient norm 
     0     5.907973e-01     1.331833e-01
 * time: 6.9141387939453125e-6
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.5986351308220609
Iter     Function value   Gradient norm 
     0     5.986351e-01     1.974054e-01
 * time: 9.059906005859375e-6
----------------------------------------------
The true error, without regularization: 0.5333686411819107
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 23.792584332824845
Iter     Function value   Gradient norm 
     0     5.945233e-01     6.110188e-02
 * time: 8.821487426757812e-6
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.6085350904973597
Iter     Function value   Gradient norm 
     0     6.085351e-01     3.755299e-01
 * time: 9.059906005859375e-6
----------------------------------------------
The true error, without regularization: 0.5350431329950823
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 22.64346573586016
Iter     Function value   Gradient norm 
     0     6.005408e-01     1.917981e-01
 * time: 8.821487426757812e-6
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.6142710352465214
Iter     Function value   Gradient norm 
     0     6.142710e-01     2.713901e-01
 * time: 5.9604644775390625e-6
----------------------------------------------
The true error, without regularization: 0.532452830023281
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 22.544608388759713
Iter     Function value   Gradient norm 
     0     6.028858e-01     9.696636e-02
 * time: 7.867813110351562e-6
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.6257929873426702
Iter     Function value   Gradient norm 
     0     6.257930e-01     4.065490e-01
 * time: 7.867813110351562e-6
----------------------------------------------
The true error, without regularization: 0.5334257045264224
----------------------------------------------
Out of time
-----------------------------------------------------------------------
target value 1.0
-----------------------------------------------------------------------
allowed time = 19.26211530220371
Iter     Function value   Gradient norm 
     0     6.090391e-01     5.020799e-02
 * time: 7.867813110351562e-6
perturbing for the 1.0th time. rate = 0.1
couldn't find a better location. Adjust stepSize or number of samples K
0.629497232244989
Iter     Function value   Gradient norm 
     0     6.294972e-01     4.065378e-01
 * time: 7.152557373046875e-6
----------------------------------------------
The true error, without regularization: 0.5441065169219705
----------------------------------------------
Out of time
