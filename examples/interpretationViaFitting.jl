using Plots
using Revise
using CUDA, Zygote, Flux, FFTW
using collatingTransform
using Test
using LinearAlgebra
pinkNoise(N; α=1) = pinkNoise(Float64, N; α=α)
function pinkNoise(t::DataType, N; α=1) 
    pink = 1 ./ (t(1) .+ (t(0):t(N>>1))).^t(α) # note: not 1:N>>1+1
    phase = exp.((t(2π)*im) .*rand(t,N>>1+1))
    res = irfft(pink .* phase, N)
    res ./norm(res)
end
init = reshape(pinkNoise(Float64, 128), (128,1,1));
init = randn(Float64,641,1,1); # random noise can't really be fit
init = reshape(sin.(range(0, 128π+π/2,length=641)), (641,1,1)); #choosing a specific frequency
init = Float64.(init);
inputSize = size(init)
st = scatteringTransform(inputSize,2,useGpu=false, dType=Float64, normalize=true)
plot([abs.(rfft([init[:,1,1]; reverse(init[:,1,1])]))/norm(fft(init[:,1,1]))
      cpu(st.mainChain[1].weight)[1:642,:]],legend=false, title="fitting vs
wavelets used")
heatmap(cpu(st.mainChain[1].weight))
typeof(st.mainChain[1].weight)
# does the gradient give the inverse?
@time res = st(init)
@time res, back = pullback(st, init)
@time returnedInit = back(res);
plot(returnedInit[1][:,1,1])
plot(abs.(fft(returnedInit[1][:,1,1])))
plot([abs.(10^13 .* fft(returnedInit[1][:,1,1])) 1/2*abs.(fft(init[:,1,1]))])
# visually, not really
res, back = pullback(scattered, map(x->x .+ 1, res.result))
size(back(res))

norm(target[pathLocs(2,:)])/19

st(init)
size(init)
function plotDescentResults(pathOfDescent, err, N,aimingAt,l)
    pHeat = heatmap([pathOfDescent[:,1,1:10:N] aimingAt[:,1,1]],title="Sequence of estimates (last is the target)",xlabel="run number", ylabel="coordinate")
    pErr = plot(log.(err),title="l2 error in scat domain", legend=false, titlefontsize=10)
    pComp = plot([abs.(fft(pathOfDescent[:,1,N])) abs.(fft(aimingAt[:,1,1]))],
                 labels=["current fit" "final goal"],legend=(.85,1),
                 legendfontsize=5, title="Fourier domain comparison",
                 titlefontsize=10)
    pErrCoord = plot([pathOfDescent[:,1,N] aimingAt[:,1,1] pathOfDescent[:,1,N] -
    aimingAt[:,1,1]], legend=false, title="space domain comparison", titlefontsize=10)
    plot(pHeat, pErr, pErrCoord, pComp, layout=l, titlefontsize=10)
end
function plotPathComparison(p,x,y, st;firstLabel="first", secondLabel="second")
    xp = st(x)[p]
    yp = st(y)[p]
end

init = randn(Float64,641,1,1);
p = pathLocs(0,(32,))
obj = makeObjFun(st(init), p, st)
obj(randn(size(init)))
initGuess = randn(Float32, size(init)...)
N = 100
opt =Momentum(1)
pathOfDescent, errr = fitReverseSt(N,initGuess, opt=opt, obj=obj);
pathOfDescent, errr = continueTrain(N,pathOfDescent,err, opt=opt, obj=obj);
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescent, errr, N, init,l)

p0 = pathLocs(0,:)
firstLabel = "descent start"; secondLabel="descent end"; thirdLabel="target"
xp = st(pathOfDescent[:,1:1,1:1])[p0]; size(yp[1])
yp = st(pathOfDescent[:,1:1,end:end])[p0]; size(xp[1])
zp = st(init[:,1:1,1:1])[p0]; size(zp[1])
plot([xp[1][:,1,1], yp[1][:,1,1], zp[1][:,1,1]], 
     labels=[firstLabel secondLabel thirdLabel], title="layer 0")
scatter!([30],[min(minimum(xp[1]), minimum(yp[1]), minimum(zp[1]))], labels="fit location")
savefig("fittinglayerZeroOneCoord.pdf")

# fitting two coordinates
init = randn(Float64,641,1,1);
p = pathLocs(0,([32, 200],))
obj = makeObjFun(st(init), p, st)
obj(randn(size(init)))
initGuess = randn(Float32, size(init)...)
N = 100
opt =Momentum(10)
pathOfDescent, errr = fitReverseSt(N,initGuess, opt=opt, obj=obj);
pathOfDescent, errr = continueTrain(N,pathOfDescent,err, opt=opt, obj=obj);
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescent, errr, N, init,l)

p0 = pathLocs(0,:)
firstLabel = "descent start"; secondLabel="descent 1000 iter"; thirdLabel="target"
xp = st(pathOfDescent[:,1:1,1:1])[p0]; size(yp[1])
yp = st(pathOfDescent[:,1:1,end:end])[p0]; size(xp[1])
zp = st(init[:,1:1,1:1])[p0]; size(zp[1])
plot([xp[1][:,1,1], yp[1][:,1,1], zp[1][:,1,1]], 
     labels=[firstLabel secondLabel thirdLabel], title="layer 0")
scatter!([30, 200], fill(min(minimum(xp[1]), minimum(yp[1]),
                             minimum(zp[1])),2),label="fit points")
savefig("fittinglayerZeroTwoCoords.pdf")


# fitting in the first layer
init = randn(Float64,641,1,1);
p = pathLocs(1,([32, 200],2))
obj = makeObjFun(st(init), p, st)
obj(randn(size(init)))
initGuess = randn(Float32, size(init)...)
N = 100
opt =Momentum(10)
pathOfDescent, errr = fitReverseSt(N,initGuess, opt=opt, obj=obj);
pathOfDescent, errr = continueTrain(N,pathOfDescent,err, opt=opt, obj=obj);
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescent, errr, N, init,l)

p0 = pathLocs(1,2)
firstLabel = "descent start"; secondLabel="descent 400 iter"; thirdLabel="target"
xp = st(pathOfDescent[:,1:1,1:1])[p0]; size(yp[1])
yp = st(pathOfDescent[:,1:1,end:end])[p0]; size(xp[1])
zp = st(init[:,1:1,1:1])[p0]; size(zp[1])
plot([xp[1][:,1,1], yp[1][:,1,1], zp[1][:,1,1]], 
     labels=[firstLabel secondLabel thirdLabel], title="layer 0")
scatter!([30, 200], fill(min(minimum(xp[1]), minimum(yp[1]),
                             minimum(zp[1])),2),label="fit points")

savefig("fittinglayerOneTwoCoords.pdf")

# fitting a range in the first layer
init = randn(Float64,641,1,1);
p = pathLocs(1,(100:180,2))
obj = makeObjFun(st(init), p, st)
obj(randn(size(init)))
initGuess = randn(Float32, size(init)...)
N = 100
opt =Momentum(10)
pathOfDescent, errr = fitReverseSt(N,initGuess, opt=opt, obj=obj);
pathOfDescent, errr = continueTrain(N, pathOfDescent, err, opt=opt, obj=obj);
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescent, errr, N, init,l)

p0 = pathLocs(1,2)
firstLabel = "descent start"; secondLabel="descent 800 iter"; thirdLabel="target"
xp = st(pathOfDescent[:, 1:1, 1:1])[p0]; size(yp[1])
yp = st(pathOfDescent[:,1:1,end:end])[p0]; size(xp[1])
zp = st(init[:,1:1,1:1])[p0]; size(zp[1])
plot([xp[1][:,1,1], yp[1][:,1,1], zp[1][:,1,1]], 
     labels=[firstLabel secondLabel thirdLabel], title="layer 1")
scatter!([100:180], fill(min(minimum(xp[1]), minimum(yp[1]),
                             minimum(zp[1])),length(100:180)),label="fit points")

savefig("fittinglayerOneRangeCoords.pdf")


# fitting a single in the second layer
init = randn(Float64,641,1,1);
p = pathLocs(2,(71,3,2))
obj = makeObjFun(st(init), p, st)
obj(randn(size(init)))
initGuess = randn(Float32, size(init)...)
N = 100
opt =Momentum(1)
pathOfDescent, errr = fitReverseSt(N,initGuess, opt=opt, obj=obj);
opt =Momentum(.01)
pathOfDescent, errr = continueTrain(N, pathOfDescent, err, opt=opt, obj=obj);
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescent, errr, N, init,l)

p0 = pathLocs(2,(3,2))
firstLabel = "descent start"; secondLabel="descent 100 iter"; thirdLabel="target"
xp = st(pathOfDescent[:, 1:1, 2:2])[p0]; size(yp[1])
yp = st(pathOfDescent[:,1:1,end:end])[p0]; size(xp[1])
zp = st(init[:,1:1,1:1])[p0]; size(zp[1])
plot([xp[1][:,1,1], yp[1][:,1,1], zp[1][:,1,1]], 
     labels=[firstLabel secondLabel thirdLabel], title="layer 2")
pnts = [71]
hline!(st(pathOfDescent[:,1:1,end:end])[p],label="")
scatter!(pnts, fill(min(minimum(xp[1]), minimum(yp[1]),
                             minimum(zp[1])),length(pnts)),label="fit points")

savefig("fittinglayerTwoSingleCoord.pdf")
plot(pathOfDescent[:,1,end])


# fitting a single in the second layer
init = randn(Float64,641,1,1);
p = pathLocs(2,(71,3,2))
obj = makeObjFun(st(init), p, st)
obj(randn(size(init)))
initGuess = randn(Float32, size(init)...)
N = 100
opt =Momentum(1)
pathOfDescent, errr = fitReverseSt(N,initGuess, opt=opt, obj=obj);
opt =Momentum(.01)
pathOfDescent, errr = continueTrain(N, pathOfDescent, err, opt=opt, obj=obj);
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescent, errr, N, init,l)

p0 = pathLocs(2,(3,2))
firstLabel = "descent start"; secondLabel="descent 100 iter"; thirdLabel="target"
xp = st(pathOfDescent[:, 1:1, 2:2])[p0]; size(yp[1])
yp = st(pathOfDescent[:,1:1,end:end])[p0]; size(xp[1])
zp = st(init[:,1:1,1:1])[p0]; size(zp[1])
plot([xp[1][:,1,1], yp[1][:,1,1], zp[1][:,1,1]], 
     labels=[firstLabel secondLabel thirdLabel], title="layer 2")
pnts = [71]
hline!(st(pathOfDescent[:,1:1,end:end])[p],label="")
scatter!(pnts, fill(min(minimum(xp[1]), minimum(yp[1]),
                             minimum(zp[1])),length(pnts)),label="fit points")

savefig("fittinglayerTwoSingleCoord.pdf")


# fitting two in the second layer
init = randn(Float64,641,1,1);
p = pathLocs(2,([25, 71, 100],3,2))
obj = makeObjFun(st(init), p, st)
obj(randn(size(init)))
initGuess = randn(Float32, size(init)...)
N = 100
opt =Momentum(1)
pathOfDescent, errr = fitReverseSt(N,initGuess, opt=opt, obj=obj);
opt =Momentum(.01)
pathOfDescent, errr = continueTrain(N, pathOfDescent, err, opt=opt, obj=obj);
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescent, errr, N, init,l)

p0 = pathLocs(2,(3,2))
firstLabel = "descent start"; secondLabel="descent 100 iter"; thirdLabel="target"
xp = st(pathOfDescent[:, 1:1, 2:2])[p0]; size(yp[1])
yp = st(pathOfDescent[:,1:1,end:end])[p0]; size(xp[1])
zp = st(init[:,1:1,1:1])[p0]; size(zp[1])
plot([xp[1][:,1,1], yp[1][:,1,1], zp[1][:,1,1]], 
     labels=[firstLabel secondLabel thirdLabel], title="layer 2")
pnts = [25, 71,100]
hline!(st(pathOfDescent[:,1:1,end:end])[p],label="")
scatter!(pnts, fill(min(minimum(xp[1]), minimum(yp[1]),
                             minimum(zp[1])),length(pnts)),label="fit points")

savefig("fittinglayerTwo3Coord.pdf")


# fitting the whole path
init = randn(Float64,641,1,1);
p = pathLocs(2,(3,2))
obj = makeObjFun(st(init), p, st)
obj(randn(size(init)))
initGuess = randn(Float32, size(init)...)
N = 1000
opt =Momentum(1)
pathOfDescent, errr = fitReverseSt(N,initGuess, opt=opt, obj=obj);
opt =Momentum(.01)
pathOfDescent, errr = continueTrain(N, pathOfDescent, err, opt=opt, obj=obj);
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescent, errr, N, init,l)

p0 = pathLocs(2,(3,2))
firstLabel = "descent start"; secondLabel="descent 100 iter";
thirdLabel="descent 1800 iter"; fourthLabel = "target"
xp = st(pathOfDescent[:, 1:1, 2:2])[p0]; size(yp[1])
yp = st(pathOfDescent[:,1:1,100:100])[p0]; size(xp[1])
zp = st(pathOfDescent[:,1:1,end:end])[p0]; size(xp[1])
ap = st(init[:,1:1,1:1])[p0]; size(zp[1])
plot([xp[1][:,1,1], yp[1][:,1,1], zp[1][:,1,1], ap[1][:,1,1]], 
     labels=[firstLabel secondLabel thirdLabel fourthLabel], title="layer 2")

pnts = [25, 71,100]
hline!(st(pathOfDescent[:,1:1,end:end])[p],label="")
scatter!(pnts, fill(min(minimum(xp[1]), minimum(yp[1]),
                             minimum(zp[1])),length(pnts)),label="fit points")

savefig("fittinglayerTwoFullPath.pdf")




##################################
# old stuff
##################################
using Flux:mse
@time dummyRes = st(init);
exampleToFit = scattered(([zeros(Float64, size(x)) for x in
                           dummyRes.result]...,));
exampleToFit[1][25,1,1] = 1
objectiveFunction(x,y) = sum(mse(a[1],a[2]) for a in zip(st(x).result,y.result))
@time objectiveFunction(init,exampleToFit)

# let's try just doing gradient descent and hope everything works out
exampleToFit = st(init); 
N = 6410
# this is for fitting some random input; similar results for the above example
pathOfDescent = zeros(Float64, 641,1,N);
pathOfDescent[:,:,1] = randn(Float64, inputSize);
(typeof(pathOfDescent), size(pathOfDescent))
∇ = gradient((x)->objectiveFunction(x,exampleToFit), pathOfDescent[:,:,1:1])

μi(i) = 10#/i^(1/2)
err = zeros(Float64, N); err[1] = objectiveFunction(pathOfDescent[:,:,1:1],exampleToFit)
for ii=2:N
    err[ii], back = pullback((x)->objectiveFunction(x,exampleToFit), pathOfDescent[:,:,(ii-1):(ii-1)])
    if ii %10==0
        println("$ii $(err[ii])")
    end
    ∇ = back(err[ii])
    pathOfDescent[:,:,ii] = pathOfDescent[:, :, ii-1] - μi(ii) .* ∇[1]
end
using Plots
heatmap([pathOfDescent[:,1,1:10:6410] init[:,1,1]],title="Convergence to a randn example (last is the target)",xlabel="run number", ylabel="coordinate") # run twice
plot(err)

# ok, so it looks like simple gradient descent only vaguely works (the convergence is quite slow)
N = 641
pathOfDescent = zeros(Float64, 641,1,N);
currentGuess = randn(Float64,641,1);
opt = Momentum()
tesst = copy(currentGuess);
target = st(init);
objectiveFunction(x,y) = sum(mse(a[1],a[2]) for a in zip(st(x),y))
using IterTools: ncycle

Flux.train!(objectiveFunction, currentGuess, ncycle([(currentGuess, target)], N), Momentum())


using Flux:update!, mse
currentGuess = randn(Float64,641,1,1); #fixing the starting location to see the dependence on other factors

N = 1000
opt = Momentum()
target = st(init);
∇ = gradient((x)->objectiveFunction(x,target), currentGuess);

pathOfDescent = zeros(Float64, 641,1,N);
objectiveFunction(x,y) = sum(mse(a[1],a[2]) for a in zip(st(x),y))
pathOfDescent = zeros(Float64, 641,1,N);
pathOfDescent[:,:,1] = currentGuess;
err = zeros(Float64, N); err[1] = objectiveFunction(pathOfDescent[:,:,1:1],exampleToFit)
for ii=2:N
    err[ii], back = pullback((x)->objectiveFunction(x,target), currentGuess)
    if ii %10==0
        println("$ii $(err[ii])")
    end
    ∇ = back(err[ii])
    update!(opt, currentGuess, ∇[1])
    pathOfDescent[:,:,ii] = currentGuess
end
objectiveFunction(currentGuess, target)
plot(heatmap([pathOfDescent[:,1,1:10:N] init[:,1,1]],title="Convergence to a randn example (last is the target)",xlabel="run number", ylabel="coordinate"), plot(err), layout=(1,2))
plot(pathOfDescent[:,1,N] .- init[:,1,1])
plot([pathOfDescent[:,1,N] init[:,1,1]])

# starting at zero
∇ = gradient((x)->objectiveFunction(x,target), currentGuess);
plot(plot(∇[1][:,1,1]), plot(init[:,1,1]))

##################################
# end old stuff
##################################
using Flux:update!, mse, Optimiser

# TODO; if this is zero everything becomes NaNs for some reason. worth noting that the gradients in the case of mostly zero were massive, so there may be stability of some sort going on
currentGuess = zeros(Float64,641,1,1); #fixing the starting location to see the dependence on other factors
currentGuess[51,1,1] = .00001; # fixing the NaN thing

N = 100
opt = Momentum(.1,1)
@time target = st(init);

@time res, back = pullback(x->st(x), init);
@time currentGuess = back(target)[1]; # try the pullback of the target as an initialization
currentGuess =currentGuess ./norm(currentGuess);
plot(currentGuess[:,1,1])

currentGuess = randn(Float64,641,1,1);
# need to figure out how to do this legitimately 
obj(x,y) = sum(mse(a[1],a[2]) for a in zip(st(x),y)) + abs(norm(x)-norm(init))

# accessing just a couple of locations
obj(x,y,locs) = sum(mse(a[1][locs]))
[size(x) for x in res]
typeof(map(x->size(x), res))
[x[]]
typeof(Colon())
supertype(supertype(Int64))
accessType = Union{Colon, <:Integer, <:AbstractArray{<:Integer}, Nothing}

function pL(indices,m=2)
    return pathLocs{m+1}(indices)
end
res[1][(1,2,5,7,9),:,:]
tmp = ((:, 1, 1), (1:30, [1, 3, 5], 1), ((1,4,2,3), 19, 20, 1))
supertype(supertype(supertype(typeof(1:4:6))))
pL(tmp)
pL(map(x->size(x), res))
#                             <:Union{NTuple{3, Int64}, 
#                                     NTuple{4, Int64},
#                                     NTuple{5, Int64} }}) where m
# end
map(x->size(x), res)
pL(map(x->size(x), res))
df(ind::Tuple{Vararg{<:Real, m}}) where m = ind
df((1.0,1))
ff(ind::NTuple{m, <:Real}) where m = ind
ff((1.0,1))

function trainThing(N, target, initGuess, opt, ifGpu=identity, obj = obj)
    ongoing = copy(initGuess|> ifGpu);
    pathOfDescent = zeros(Float64, size(ongoing,1),1,N)|>ifGpu;
    println("initial objective function is $(obj(ongoing,target))")
    pathOfDescent[:,:,1] = ongoing|>ifGpu;
    err = zeros(Float64, N); err[1] = obj(pathOfDescent[:,:,1:1],target)
    for ii=2:N
        err[ii], back = pullback((x)->obj(x,target), ongoing)
        if err[ii]>1e5 || isnan(err[ii])
            println("$(err[ii]) way too big at $ii, aborting")
            break
        end
        if ii %10==0
            println("$ii $(err[ii])")
        end
        ∇ = back(err[ii])
        update!(opt, ongoing, ∇[1])
        pathOfDescent[:,:,ii] = ongoing
    end
    return (pathOfDescent, err)
end
N = 1000
target = st(init);
size(init)
pathOfDescent, err = trainThing(N, target, currentGuess, Nesterov(1));
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescent, err, N,init, l)

pathOfDescent, err = trainThing(N, target, currentGuess, Momentum());
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescent, err, N,init, l)
pathOfDescentTmp, errTmp = continueTrain(100, target, pathOfDescent, err, Momentum(10));
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescentTmp, errTmp, N,init, l)
pathOfDescentTmp, errTmp = continueTrain(1000, target, pathOfDescentTmp, errTmp, Descent(10000));
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescentTmp, errTmp, N,init, l)

pathOfDescentTmpp, errTmpp = continueTrain(100, target, pathOfDescentTmp, errTmp, Descent(10000));
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescentTmpp, errTmpp, N,init, l)

@time currentGuess = back(target)[1]; # try the pullback of the target as an initialization
currentGuess =currentGuess ./norm(currentGuess);

currentGuess = randn(Float64, 641,1,1);
# Gradient Descent alone
N = 1000
target = st(init);
size(init)
pathOfGrad, errGrad = trainThing(N, target, currentGuess, Descent(10000));
l = @layout [a; [b; c] d]
plotDescentResults(pathOfGrad, errGrad, N,init, l)
pathOfGradTmp, errGradTmp = continueTrain(500, target, pathOfGrad, errGrad, Descent(1000));
l = @layout [a; [b; c] d]
plotDescentResults(pathOfGradTmp, errGradTmp, N,init, l)
pathOfGradTmp, errGradTmp = continueTrain(500, target, pathOfGradTmp, errGradTmp, Descent(100));
# Gradient Descent exp decay
N = 1000
target = st(init);
size(init)
pathOfGrad, errGrad = trainThing(N, target, currentGuess,
                                 Optimiser(ExpDecay(10, .9, 10),
                                           Descent()));
l = @layout [a; [b; c] d]
plotDescentResults(pathOfGrad, errGrad, N,init, l)
pathOfGradTmp, errGradTmp = continueTrain(500, target, pathOfGrad, errGrad, Descent(1000));
plot(log.(errGrad),xticks=0:100:1000)

# Gradient Descent exp decay fitting noise
N = 1000
initTarg = randn(Float64,641,1,1)
target = st(initTarg);
size(init)
initGuess = randn(Float64,641,1,1);
pathOfGrad, errGrad = trainThing(N, target, initGuess,
                                 Optimiser(ExpDecay(10, .9, 10),
                                           Descent()));
l = @layout [a; [b; c] d]
plotDescentResults(pathOfGrad, errGrad, N,initTarg, l)
pathOfGradTmp, errGradTmp = continueTrain(500, target, pathOfGrad, errGrad, Descent(1000));


# ADAM
N = 1000
target = st(init);
size(init)
pathOfAdam, errAdam = trainThing(N, target, currentGuess, ADAM(.1));
l = @layout [a; [b; c] d]
plotDescentResults(pathOfAdam, errAdam, N,init, l)
plot(currentGuess[:,1,1])

#pathOfDescent, err = pathOfDescentTmp, errTmp

function trainThing(N, target, initGuess, opt, ifGpu=identity, obj = obj)
    ongoing = copy(initGuess|> ifGpu);
    pathOfDescent = zeros(Float64, size(ongoing,1),1,N)|>ifGpu;
    println("initial objective function is $(obj(ongoing))")
    pathOfDescent[:,:,1] = ongoing|>ifGpu;
    err = zeros(Float64, N); err[1] = obj(pathOfDescent[:,:,1:1])
    for ii=2:N
        err[ii], back = pullback((x)->obj(x), ongoing)
        if err[ii]>1e5 || isnan(err[ii])
            println("$(err[ii]) way too big at $ii, aborting")
            break
        end
        if ii %10==0
            println("$ii $(err[ii])")
        end
        ∇ = back(err[ii])
        update!(opt, ongoing, ∇[1])
        pathOfDescent[:,:,ii] = ongoing
    end
    return (pathOfDescent, err)
end
function continueTrain(N, target, pathOfDescent, err, opt, ifGpu=identity, obj=obj)
    prevLen = size(pathOfDescent,3)
    currentGuess = pathOfDescent[:,:,end:end] |> ifGpu;
    pathOfDescent = cat(pathOfDescent, 
                        zeros(Float64, size(pathOfDescent,1),1,N), 
                        dims=3) |> ifGpu;
    err = cat(err, zeros(Float64, N), dims=1)
    for ii in (prevLen+1):(prevLen+N)
        err[ii], back = pullback((x)->obj(x,target), currentGuess)
        if err[ii]>1e5 || isnan(err[ii])
            println("way too big at ii, aborting")
            break
        end
        if ii %10==0
            println("$ii $(err[ii])")
        end
        ∇ = back(err[ii])
        update!(opt, currentGuess, ∇[1])
        pathOfDescent[:,:,ii] = currentGuess
    end
    return (pathOfDescent, err)
end


function plotDescentResults(pathOfDescent, err, N,init,l)
    pHeat = heatmap([pathOfDescent[:,1,1:10:N] init[:,1,1]],title="Sequence of estimates (last is the target)",xlabel="run number", ylabel="coordinate")
    pErr = plot(log.(err),title="l2 error in scat domain", legend=false, titlefontsize=10)
    pComp = plot([abs.(fft(pathOfDescent[:,1,N])) abs.(fft(init[:,1,1]))],
                 labels=["current fit" "final goal"],legend=(.85,1),
                 legendfontsize=5, title="Fourier domain comparison",
                 titlefontsize=10)
    pErrCoord = plot([pathOfDescent[:,1,N] init[:,1,1] pathOfDescent[:,1,N] -
    init[:,1,1]], legend=false, title="space domain comparison", titlefontsize=10)
    plot(pHeat, pErr, pErrCoord, pComp, layout=l, titlefontsize=10)
end


[size(x) for x in target]
plot(target[1][:,1,1])
heatmap(target[3][:,:,20,1])

p0 = plot(target[1][:,1,1], title="layer 0")
p1 = heatmap(target[2][:,:,1], title="layer 1",xlabel = "wavelet index", ylabel="time")

coordWeight = [norm(target[3][:,i,j,1]) for i in 1:size(target[3],2), j in 1:size(target[3],3)];
p2 = heatmap(coordWeight, title="layer 2 norms", xlabel = "layer 1 index", ylabel="layer 2 index")
l = @layout [a; b c]
plot(p0,p1,p2,layout=l)

t = st(init);
coordWeight = [norm(t[3][:,i,j,1]) for i in 1:size(t[3],2), j in 1:size(t[3],3)];
heatmap(coordWeight, title="layer 2",xlabel = "layer 1 index", ylabel="layer 2 index")
p


#######################################################################################
# lets see whether we can fit some/all of the cylinder etc examples
#######################################################################################
include("../simpleSignalExamples.jl")
data = reshape(generateData(100), (128,1,300));
st = scatteringTransform(size(data), 2, useGpu=false, dType=Float64, normalize=true, decreasings=[2,3,2],averagingLengths=[1,2,4])
using Wavelets, FFTW
stOne = scatteringTransform((128,1,1), 2, σ=abs,useGpu=false, dType=Float64,
                            normalize=true, decreasings=[π,1,1], cw=WT.morl, 
                            averagingLengths=[4,3,4])
wave1,wave2,wave3 = getWavelets(stOne)
plot(wave1')
plot([abs.(wave1) sum(abs.(wave1),dims=2)])
size(wave1)
ω = range(0.01,stop=2π, length = size(wave1,1))
plot((wave1[:,1:end-1] ./ (im .* ω).^1)')
integ = [zeros(size(wave1,1)-1, size(wave1,2)-1); wave1[:,1:end-1] ./ (im .* ω).^3]
plot(ifftshift(ifft(integ, 1), 1)')
plot(real.(ifftshift(ifft(integ, 1),1))[:,1])
maximum(abs.(integ))
heatmap(abs.(integ))
plot(integ')
plot(ifftshift(irfft(wave1,128*2,1),1))
stw = stOne(data[:,:,33:33])
heatmap(stw[1][:,:,1])
anim = Animation()
for j=1:18
    heatmap(stw[2][:,j,:,1], ylabel="layer 1 coord", 
            xlabel="space location", title="layer 2 result j=$j")
    frame(anim)
end
gif(anim,"cylinderSecondCoords.gif",fps=1)

j=2; heatmap(stw[2][:,j,:,1], ylabel="layer 1 coord", 
        xlabel="space location", title="layer 2 result j=$j")

plot(plot(data[:,1,33]), plot(stw[1][:,1,1]),layout=(2,1))
argmax(stw[2])

p = pathLocs(2, (15:20,2,25))
pLoc = pathLocs(2, (2,25))
plot(stw[p])
obj = makeObjFun(stw, p, stOne,false)
initGuess = randn(Float64,128,1,1);
oneThatSuperPlateaued = initGuess;
initGuess = zeros(Float64,128,1,1);
opt = Momentum(.0001,1); N = 300
opt = Optimiser(InvDecay(1e-3), Descent(.01)); N = 300
opt = Descent(.0001); N = 300
opt = Momentum(.00001); N = 300
opt = ADAM(); N = 300

opt = Descent(.0001); N = 300  # keepWithin=norm(data[:,:,33]), 
pathOfDescent, err = fitReverseSt(N,initGuess, opt=opt, obj=obj,
                                  keepWithin=norm(data[:,:,33]), stochastic=false); plot(log.(err))
getGradPath(pathOfDescent,obj) = cat([gradient(obj,reshape(x,(size(x)...,1)))[1] for x in
                                eachslice(pathOfDescent,dims=3)]..., dims=3)
gradAlongPath = getGradPath(pathOfDescent,obj);
sum(mse(a[1],a[2]) for a in zip(stOne(pathOfDescent[:,:,end:end])[p],stw[p]))
size(gradAlongPath)
heatmap(gradAlongPath[:,1,:])
plot(pathOfDescent[:,:,end])
plot(heatmap(pathOfDescent[:,1,:]), plot(log.(err)))
plot(pathOfDescent[:,1,end-5])
plot([norm(x) for x in eachslice(pathOfDescent, dims=3)])
plot([stw[pLoc] stOne(pathOfDescent[:,:,end:end])[pLoc] stOne(pathOfDescent[:,:,1:1])[pLoc]], labels=["target" "end" "start"])
# what happens if we shift the signal?
pathOfDescent[:,:,end:end] = circshift(pathOfDescent[:,:,end:end], (-10,0)); 
plot([stw[p] stOne(pathOfDescent[:,:,end:end])[p] stOne(pathOfDescent[:,:,1:1])[p]], labels=["target" "end" "start"])
opt = Descent(.000001); N = 300
pathOfDescent, err = continueTrain(N,pathOfDescent, err, opt=opt, obj=obj,
                                   keepWithin=norm(data[:,:,33]), stochastic=false); plot(log.(err))

transformPath(pod, p,st) = cat([stOne(reshape(x,(size(x)...,1)))[p] for x in
                                eachslice(pod,dims=3)]...,dims=2)
pathOfRes = transformPath(pathOfDescent, p, stOne);
plot(stw[p],color=:reds, legend=false,linewidth=3)
plot!(pathOfRes, line_z=([1:size(pathOfRes,2)...])',
      color=:darkrainbow, legend=false, colorbar=true)
plot(pathOfDescent[:,1,1])




size(stw[2])
heatmap(stw[2][:,:,1,1])
p1 = plot(stw[2][:,1,1,1],legend=false)

plts = map((ij)->plot(stw[2][:,ij[1],
                             ij[2],1],legend=false),Iterators.product(1:18,1:28))
size(plts)
n,m = size(stw[2])[2:3]
plot!(stw[2][:,1,1,1],legend=false,subplot=2,ticks=nothing,inset=(1,bbox(0,0,1/(m+3),1/(n+3),:bottom,:left)))
(m-1 )/m
gr(size=2.5 .*(280,180))

plt = heatmap([norm(stw[2][:,i,j,1]) for i=1:n,j=1:m],yticks=1:n,xticks=1:m,tick_direction=:out)
plot!(stw[2][:,2,1,1],legend=false,subplot=2,ticks=nothing,inset=(1,bbox(xrange[end]
                             ,.945,Δx/(m+10),1/(n+3),:bottom,:left)))

plotSecondLayer(stw,"Second Layer Cylinder 33")
savefig("gradientFigures/SecondLayerCyl33.png")
plot(data[:,1,105])
plotSecondLayer(stOne(data[:,:,34:34]),"Second Layer Cylinder 34")
savefig("gradientFigures/SecondLayerCyl34.png")
savefig("gradientFigures/SecondLayerCyl34.pdf")
plotSecondLayer(stOne(data[:,:,105:105]),"Second Layer Bell 105")
savefig("gradientFigures/SecondLayerBell105.pdf")
plotSecondLayer(stOne(data[:,:,133:133]),"Second Layer Bell 133")
savefig("gradientFigures/SecondLayerBell133.pdf")


plot(plts[1:10,1:4]..., layout = (10,4))
size(stw[2])[2:end-1])




initGuess = randn(128,1,1)
initGuess = ones(128,1,1)
tmpEx = similar(stOne(initGuess))
p = pathLocs(2, (14,4,11))
mkdir("gradientFigures/Fitting_4_11")
tmpEx[p] = 18*28*ones(1)/5
obj = makeObjFun(tmpEx, stOne, false)
opt = Momentum(1e-5); N = 200
@time descent, err = fitReverseSt(N, initGuess; opt=opt, obj=x->obj(x,p), keepWithin=10);plot(log.(err), legend=false)
opt = Descent(1e-3); N = 400
@time descentTmppp, errTmppp = continueTrain(N, descentTmpp, errTmpp; opt=opt, obj=x->obj(x,p), keepWithin=10);plot(log.(errTmp), legend=false)
plot(log.(errTmppp), legend=false)
plot(log.(err), legend=false,title="descent",ylabel="log error", xlabel="iteration")
savefig("gradientFigures/Fitting_4_11/descent.pdf")
# comparing the gradient of (11,3) with that of (11,4) and the current fitted function
pPrev = pathLocs(2,(14,3,11))
plot(gradient(x->stOne(x)[pPrev][1], descentTmp[:,1:1,end:end])[1][:,1,1], label="gradient (11,3)")
plot!(gradient(x->obj(x,p), descentTmp[:,1:1,end:end])[1][:,1,1],label="gradient(11,4)")
plot!(descentTmp[:,1,end],label="signal")
# log trajectory of the gradient over the descent
heatmap(log.(abs.(diff(descentTmp[:,1,:],dims=2))))
heatmap(descentTmp[:,1,:]',title="Descent Trajectory", ylabel="iteration", xlabel="space")
plot(plot(descent[:,1,end],legend=false, title="fitting (11,4) Space"), plot(abs.(rfft(descent[:,1,end:end], 1)),legend=false,title="positive half of the Fourier coefficients"),layout=(2,1))
savefig("gradientFigures/Fitting_4_11/ActuallyFitPlot.pdf")
heatmap(abs.(rfft(descent[:,1,:], 1)'), title="Descent Trajectory", ylabel="iteration", xlabel="space")
p1 = heatmap(stOne(descent[:,1:1,end:end])[1][:,:,1]', title="First Layer Coordinates", xlabel="space",ylabel="frequency")
savefig("gradientFigures/Fitting_4_11/first_layer_coords.pdf")
[stOne(descent[:,1:1,end:end])[p][1,:] tmpEx[p][1,:]]
plotSecondLayer(stOne(descentTmp[:,1:1,end:end]), xVals=(.0005, .886), yVals=(0.0, .995), title="Second Layer Fitting single coordinate from all ones")
savefig("gradientFigures/Fitting_4_11/secondLayerPlot.pdf")
plotSecondLayer(stOne(initGuess),xVals=(.0005, .886),yVals=(0.0, .995),title="Second Layer Constant")







# what is the gradient of delta functions?
loc = 54
testFun = zeros(128,1,1); testFun[loc] = 1.0;
plot(testFun[:,1,1])
res = stOne(testFun)
heatmap(res[1][:,:,1]',title="First Layer Delta Function Coefficients", ylabel="Frequency Index", xlabel="Space Index")
savefig("gradientFigures/Deltas/dog/one_l54_firstLayerOutput.pdf")
heatmap(res[2][:,15,:]')
plotSecondLayer(res, title="Second Layer Delta Function Coefficients")
savefig("gradientFigures/Deltas/dog/one_l54_secondLayerOutput.pdf")
∇ = collatingTransform.∇st(stOne, testFun) 
heatmap(∇[1][:, round(Int,54/128*42), :]',title="Delta Function at 54, gradient of approximately that location", xlabel="location", ylabel="frequency index")
savefig("gradientFigures/Deltas/dog/one_l54_FirstLayer.png")
13+round(Int,54/128*42)
heatmap(∇[1][:, 13+round(Int,54/128*42), :]',title="Delta Function at 54, gradient of 31", xlabel="location", ylabel="frequency index")
savefig("gradientFigures/Deltas/dog/one_l54_FirstLayerOffSite.png")
i=1; heatmap(log.(abs.(∇[2][:, round(Int,28*54/128), i,:]')),title="Second Layer Fixed at $(i), Delta Signal at 54", xlabel="location", ylabel="Layer 1 Frequency index")
savefig("gradientFigures/Deltas/dog/one_l54_SecondLayerFixed$(i).png")
heatmap(log.(abs.(∇[2][:, round(Int,28*54/128), 9,:]')))

sct = scattered((∇[0],∇[1],(reshape(∇[2][:,round(Int,28*54/128), :, :], (128,18,28,1)))))
plotSecondLayer(sct, title="Second Layer derivatives at a delta function at 54 out of 128")
savefig("gradientFigures/Deltas/dog/one_l54_SecondLayerBoth.pdf")

size(rfft(∇[2][:,round(Int,28*54/128), :, :], 1))
sctFft = scattered((∇[0],∇[1],reshape(abs.(rfft(∇[2][:,round(Int,28*54/128), :, :], 1)), (65,18,28,1))))
plotSecondLayer(sctFft, title="Second Layer derivatives at a delta function Fourier Domain")
savefig("gradientFigures/Deltas/dog/one_l54_SecondLayerFourier.pdf")


# what if we smooth out the delta function slightly?
testFun = zeros(128,1,1); testFun[loc] = 1.0; testFun[loc-1]=.75; testFun[loc+1] = .75; testFun[loc-2]=.5; testFun[loc+2] = .5;
plot(testFun[:,1,1])
res = stOne(testFun)
heatmap(res[1][:,:,1]',title="First Layer Smoothed Delta Function Coefficients", ylabel="Frequency Index", xlabel="Space Index")
savefig("gradientFigures/Deltas/five1.75.5_l54_firstLayerOutput.pdf")
heatmap(res[2][:,15,:]')
plotSecondLayer(res, title="Second Layer Smoothed Delta Function Coefficients")
savefig("gradientFigures/Deltas/five1.75.5_l54_secondLayerOutput.png")
∇ = collatingTransform.∇st(stOne, testFun) 
heatmap(∇[1][:, round(Int,54/128*42), :]',title="Delta Function at 54, gradient of approximately that location", xlabel="location", ylabel="frequency index")
savefig("gradientFigures/Deltas/five1.75.5_l54_FirstLayer.png")
13+round(Int,54/128*42)
heatmap(∇[1][:, 13+round(Int,54/128*42), :]',title="Delta Function at 54, gradient of 31", xlabel="location", ylabel="frequency index")
savefig("gradientFigures/Deltas/five1.75.5_l54_FirstLayerOffSite.png")
i=18; heatmap(log.(abs.(∇[2][:, round(Int,28*54/128), i,:]')),title="Second Layer Fixed at $(i), Delta Signal at 54", xlabel="location", ylabel="Layer 1 Frequency index")
savefig("gradientFigures/Deltas/five1.75.5_l54_SecondLayerFixed$(i).png")
heatmap(log.(abs.(∇[2][:, round(Int,28*54/128), 9,:]')))

sct = scattered((∇[0],∇[1],(reshape(∇[2][:,round(Int,28*54/128), :, :], (128,18,28,1)))))
plotSecondLayer(sct, title="Second Layer derivatives at a smoothed delta function at 54 out of 128")
savefig("gradientFigures/Deltas/five1.75.5_l54_SecondLayerBoth.png")

size(rfft(∇[2][:,round(Int,28*54/128), :, :], 1))
sctFft = scattered((∇[0],∇[1],reshape(abs.(rfft(∇[2][:,round(Int,28*54/128), :, :], 1)), (65,18,28,1))))
plotSecondLayer(sctFft, title="Second Layer derivatives at a smoothed delta function Fourier Domain")
savefig("gradientFigures/Deltas/five1.75.5_l54_SecondLayerFourier.png")
savefig("gradientFigures/Deltas/five1.75.5_l54_SecondLayerFourier.pdf")
plot(abs.(rfft(testFun[:,1,1])))




# step function  
testFun = zeros(128,1,1); testFun[loc:end] .= 1.0; 
plot(testFun[:,1,1])
res = stOne(testFun)
heatmap(res[1][:,:,1]',title="First Layer Smoothed Delta Function Coefficients", ylabel="Frequency Index", xlabel="Space Index")
savefig("gradientFigures/Deltas/dog/step_l54_firstLayerOutput.pdf")
savefig("gradientFigures/Deltas/dog/step_l54_firstLayerOutput.png")
heatmap(res[2][:,15,:]')
plotSecondLayer(res, title="Second Layer Step Function Coefficients")
savefig("gradientFigures/Deltas/dog/step_l54_secondLayerOutput.png")
savefig("gradientFigures/Deltas/dog/step_l54_secondLayerOutput.pdf")
∇dog/step = collatingTransform.∇st(stOne, restFun) 
heatmap(∇dog/step[1][:, round(Int,54/128*42), :]',title="Delta Function at 54, gradient of approximately that location", xlabel="location", ylabel="frequency index")
savefig("gradientFigures/Deltas/dog/step_l54_FirstLayer.png")
13+round(Int,54/128*42)
heatmap(∇dog/step[1][:, 13+round(Int,54/128*42), :]',title="Delta Function at 54, gradient of 31", xlabel="location", ylabel="frequency index")
savefig("gradientFigures/Deltas/dog/step_l54_FirstLayerOffSite.png")
i=18; heatmap(log.(abs.(∇dog/step[2][:, round(Int,28*54/128), i,:]')),title="Second Layer Fixed at $(i), Delta Signal at 54", xlabel="location", ylabel="Layer 1 Frequency index")
savefig("gradientFigures/Deltas/dog/step_l54_SecondLayerFixed$(i).png")
heatmap(log.(abs.(∇dog/step[2][:, round(Int,28*54/128), 9,:]')))

sct = scattered((∇step[0],∇[1],(reshape(∇[2][:,round(Int,28*54/128), :, :], (128,18,28,1)))))
plotSecondLayer(sct, title="Second Layer derivatives at a smoothed delta function at 54 out of 128")
savefig("gradientFigures/Deltas/step_l54_SecondLayerBoth.png")

size(rfft(∇step[2][:,round(Int,28*54/128), :, :], 1))
sctFft = scattered((∇step[0],∇[1],reshape(abs.(rfft(∇[2][:,round(Int,28*54/128), :, :], 1)), (65,18,28,1))))
plotSecondLayer(sctFft, title="Second Layer derivatives at a smoothed delta function Fourier Domain")
savefig("gradientFigures/Deltas/step_l54_SecondLayerFourier.png")
savefig("gradientFigures/Deltas/step_l54_SecondLayerFourier.pdf")


t = range(-10,stop=10,length=128)
# Two steps of different regularities at different locations
f(x, a, s) = tanh.( s * (range(-a-10,stop=-a+10,length=x)))
plot(t, [f(128,-7,19) f(128, -3, 19) f(128,0,19)] .+ f(128,3,19), title="Successive step functions", legend=:bottomright, labels=["distance 10" "distance 6" "distance 3"])
savefig("gradientFigures/Deltas/two_step_actualFunctions.pdf")
savefig("gradientFigures/Deltas/two_step_actualFunctions.png")

function twoStepFunctions(p1,p2,stOne, extraName="")
    testFun = reshape(f(128,p1,19) .+ f(128,p2,19), (128,1,1));
    plot(t, testFun[:,1,1], title="Successive step functions at $p1,$p2", legend=false)
    savefig("gradientFigures/Deltas/$(extraName)two_step_l$p1$(p2)_actualFunctions.pdf")
    savefig("gradientFigures/Deltas/$(extraName)two_step_l$p1$(p2)_actualFunctions.png")
    mc = stOne.mainChain
    heatmap(mc[1](testFun)[:,1:end-1,1]',title="Successive Step functions Wavelet Transform at $p1,$p2", xlabel="space",ylabel="frequency")
    savefig("gradientFigures/Deltas/$(extraName)two_step_l$p1$(p2)_justWavelet.pdf")
    savefig("gradientFigures/Deltas/$(extraName)two_step_l$p1$(p2)_justWavelet.png")
    res = stOne(testFun)
    heatmap(res[1][:,:,1]',title="First Layer Smooth Staircase Coefficients at $p1,$p2", ylabel="Frequency Index", xlabel="Space Index")
    savefig("gradientFigures/Deltas/dog/$(extraName)twoStep_l$p1$(p2)_firstLayerOutput.pdf")
    savefig("gradientFigures/Deltas/dog/$(extraName)twoStep_l$p1$(p2)_firstLayerOutput.png")
    plotSecondLayer(res, title="Second Layer Smooth Staircase Coefficients at $p1,$p2")
    savefig("gradientFigures/Deltas/dog/$(extraName)twoStep_l$p1$(p2)_secondLayerOutput.png")
    savefig("gradientFigures/Deltas/dog/$(extraName)twoStep_l$p1$(p2)_secondLayerOutput.pdf")
end

twoStepFunctions(-3,3, stOne) 
twoStepFunctions(-1,1, stOne, "abs") 

∇step = collatingTransform.∇st(stOne, restFun) 
heatmap(∇step[1][:, round(Int,54/128*42), :]',title="Smooth Staircase at 54, gradient of approximately that location", xlabel="location", ylabel="frequency index")
savefig("gradientFigures/Deltas/dog/twoStep_l03_FirstLayer.png")
13+round(Int,54/128*42)
heatmap(∇step[1][:, 13+round(Int,54/128*42), :]',title="Smooth Staircase at 54, gradient of 31", xlabel="location", ylabel="frequency index")
savefig("gradientFigures/Deltas/dog/twoStep_l03_FirstLayerOffSite.png")
i=18; heatmap(log.(abs.(∇step[2][:, round(Int,28*54/128), i,:]')),title="Second Layer Fixed at $(i), Delta Signal at 54", xlabel="location", ylabel="Layer 1 Frequency index")
savefig("gradientFigures/Deltas/dog/twoStep_l03_SecondLayerFixed$(i).png")
heatmap(log.(abs.(∇step[2][:, round(Int,28*54/128), 9,:]')))

sct = scattered((∇step[0],∇[1],(reshape(∇[2][:,round(Int,28*54/128), :, :], (128,18,28,1)))))
plotSecondLayer(sct, title="Second Layer derivatives at a smoothed delta function at 54 out of 128")
savefig("gradientFigures/Deltas/twoStep_l03_SecondLayerBoth.png")

size(rfft(∇step[2][:,round(Int,28*54/128), :, :], 1))
sctFft = scattered((∇step[0],∇[1],reshape(abs.(rfft(∇[2][:,round(Int,28*54/128), :, :], 1)), (65,18,28,1))))
plotSecondLayer(sctFft, title="Second Layer derivatives at a smoothed delta function Fourier Domain")
savefig("gradientFigures/Deltas/twoStep_l-73_SecondLayerFourier.png")
savefig("gradientFigures/Deltas/twoStep_l-73_SecondLayerFourier.pdf")

plot(testFun[:,1,1] .+ .1randn(128))
wave1,wave2,wave3 = getWavelets(stOne)
plot(wave1')
# just doing the wavelet transform
mc = stOne.mainChain
plot(ifftshift(irfft(1/(im)^2 * mc[1].weight[:,end], 128*2)))
heatmap(abs.(fft(mc[1](testFun)[:,4:end-1,1,1], 1))')
tmp = mc[4](mc[3](mc[2](mc[1](testFun))))
size(tmp)
tmpOut = tmp[map(x->Colon(), 1:1)..., end, map(x->Colon(), 1:ndims(tmp)-1-1)...]
plot(mc[4].weight')
heatmap(tmpOut[:,:,1]')
heatmap(abs.(Float64.(mc[3](mc[2](mc[1](testFun)))[:,1:end-1,1,1])'))
(mc[2](stOne.mainChain[1](.1 .* randn(128) .+ testFun)))
import NNlib.relu
relu(x::C) where C<:Complex = real(x) > 0 ? x : C(0)
size(abs.(relu.(stOne.mainChain[1](testFun)[:,4:end-1,1,1])'))
using RecipesBase, ColorSchemes
@recipe function plotComplex(::Type{T}, val::T; c=:phase) where {T<:AbstractArray{<:Complex, 2}}
    r = abs.(val)
    r ./= maximum(r)
    θ = (angle.(val) .+ π) ./ (2π)
    phaseColor = get(colorschemes[c], θ)
    y := r .* phaseColor 
    seriestype := :image
    r .* phaseColor
end
pyplot()
# 1/(x+i)^2 is a wavelet, apparently
plot(abs.(1 ./ ((-10:.01:10) .+ im).^2))
relu.(stOne.mainChain[1](testFun)[:,1:end-1,1,1])'
plot(relu.(stOne.mainChain[1](testFun)[:,1:end-1,1,1])', title="Wavelet transform (color gives phase)", xlabel="space index", ylabel="frequency index")

t = range(-10,stop=10,length=128)
g(x, a, s) = exp.(-range(-a-10,stop=10-a,length=x).^2/s)
plot(t, [g(128,-7,1/9) g(128, -3, 1/9) g(128,1,1/9)] .+ g(128,3,1/9), title="Successive step functions", legend=:bottomright, labels=["distance 10" "distance 6" "distance 2"])
savefig("gradientFigures/Deltas/deltas_actualSignals.pdf")
savefig("gradientFigures/Deltas/deltas_actualSignals.png")
testFun = reshape(g(128,-7,1/9) .+ g(128,3,1/9), (128,1,1));
plot(t, testFun[:,1,1], legend=false,title="Double Deltas  distance 10")
savefig("gradientFigures/Deltas/deltas_l-73_actualSignal.pdf")
savefig("gradientFigures/Deltas/deltas_l-73_actualSignal.png")
res = stOne(testFun)
heatmap(res[1][:,:,1]', title="First Layer Double Delta Coefficients  distance 10", ylabel="Frequency Index", xlabel="Space Index")
savefig("gradientFigures/Deltas/deltas_l-73_firstLayerOutput.pdf")
savefig("gradientFigures/Deltas/deltas_l-73_firstLayerOutput.png")
plotSecondLayer(res, title="Second Layer Double Delta Coefficients  distance 10")
savefig("gradientFigures/Deltas/deltas_l-73_secondLayerOutput.png")
savefig("gradientFigures/Deltas/deltas_l-73_secondLayerOutput.pdf")

topoSin = sin.(1 ./ (range(-0.50001,stop=0.50001,length=128)))
plot(topoSin)
plot(relu.(stOne.mainChain[1](reshape(topoSin,(128,1,1)))[:,1:end-1,1,1])')
heatmap(abs.(stOne.mainChain[1](reshape(topoSin,(128,1,1)))[:,1:end-1,1,1]'))



function αBound(v,α,t,s,C=.1)
    if abs(v-t) < C*s 
        return s^(α+1/2) * (1+abs(t-v)^α/s^α)
    else
        return 0.0
    end
end
T = range(-1,stop=1,length=128)
S = range(0,stop=5,length=128)
bounds = [αBound.(-1,1,t,s) .+ αBound.(0.5,1,t,s) .+ αBound.(0.0,1,t,s) for t =T, s=S]; heatmap(T,S,bounds')
