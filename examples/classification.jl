using Revise
using CUDA, Zygote, Plots, cartoonLike, FFTW, LinearAlgebra, collatingTransform, Flux, LinearAlgebra
using Flux: mse, crossentropy, @epochs, onehotbatch, throttle, onecold
using Base.Iterators: partition
using MLDatasets, Dates
using Plots, Images, Statistics
using JLD2
saveCheckpointsTo = "/Elysium/dsweber/data/collatingTransform/scatteringTransform/MNIST_Checkpoints"
println("loaded the packages")
train_x, train_y = MNIST.traindata()
test_x, test_y = MNIST.testdata()
train_x = reshape(train_x, (28,28,1,60000))
trainLabels = onehotbatch(train_y, 0:9);
batchSize = 40
train = gpu.([(train_x[:, :, :, i], trainLabels[:,i]) for i in partition(1:(size(train_x,4)-1000), batchSize)]);
nClasses = 10
m=2
stack = Chain(scatteringTransform((28,28,1,batchSize), m, poolBy=6//5),
              x -> flatten(x),
              Dense(13537, 10, identity), softmax) |> gpu
valset = collect(49001:50000)
valX = train_x[:,:,:,valset] |> gpu;
valY = trainLabels[:, valset] |> gpu;
loss(x, y) = crossentropy(stack(x), y)
accuracy(x, y) = mean(onecold(x, 0:9) .== onecold(y, 0:9))


startTime = now()
evalcb = throttle(1*120) do
    acc = accuracy(batchOff(stack, valX, batchSize), valY)
    @show(acc)
    tmp = params(cpu(stack))
    JLD2.@save "$(saveCheckpointsTo)/$(startTime)/model-checkpoint$(now()).jld2" tmp acc
end
println("loaded the data fully")

opt = ADAM()


println("Initial accuracy = $(accuracy(batchOff(stack, valX, batchSize), valY))")
Params(stack) = gpu(tmp)
mkpath("$(saveCheckpointsTo)/$(startTime)")
tmp = params(cpu(stack))

JLD2.@save "$(saveCheckpointsTo)/$(startTime)/start-checkpoint$(now()).jld2" tmp
evalcb()
println("saved the first checkpoint")
Params(stack)
size(valY)

@time loss(valX[:,:,:,1:batchSize], valY[:, 1:batchSize])
evalcb()
@epochs 10 Flux.train!(loss, params(stack), train, opt, cb=evalcb)


JLD2.@save "$(saveCheckpointsTo)/$(startTime)/end-checkpoint$(startTime).jld2" tmp
