# Setting up the random shift distributions
using Random, Distributions
function generateData(N)
    d1=DiscreteUniform(16,32); d2=DiscreteUniform(32,96);

    # Making cylinder signals
    cylinderNew=zeros(128,N);
    a=rand(d1,N); b=a+rand(d2,N);
    η=randn(100);
    for k=1:100
        cylinderNew[a[k]:b[k],k]=(6+η[k])*ones(b[k]-a[k]+1);
    end
    cylinderNew += randn(128,N);     # adding noise

    # Making bell signals
    bellNew=zeros(128,N);
    a=rand(d1,N); b=a+rand(d2,N);
    η=randn(100);
    for k=1:100
        bellNew[a[k]:b[k],k]=(6+η[k])*collect(0:(b[k]-a[k]))/(b[k]-a[k]);
    end
    bellNew += randn(128,N);         # adding noise

    # Making funnel signals
    funnelNew=zeros(128,N);
    a=rand(d1,N); b=a+rand(d2,N);
    η=randn(100);
    for k=1:100
        funnelNew[a[k]:b[k],k]=(6+η[k])*collect((b[k]-a[k]):-1:0)/(b[k]-a[k]);
    end
    funnelNew += randn(128,N);       # adding noise


    test = cat(cylinderNew, bellNew, funnelNew, dims=2);
end
