using Revise
using collatingTransform, CUDA, LinearAlgebra
batchSize = 10
m=2
st = scatteringTransform((28,28,1,batchSize), m, poolBy=6//5)

x = cu(randn(28,28,1,10));
initialOutput = st(x);
f(x) = norm(st(x), 1)
