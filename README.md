# collatingTransform
[![Stable](https://img.shields.io/badge/docs-stable-blue.svg)](https://dsweber2.github.io/collatingTransform.jl/stable)
[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://dsweber2.github.io/collatingTransform.jl/dev)
[![Build Status](https://travis-ci.com/dsweber2/collatingTransform.jl.svg?branch=master)](https://travis-ci.com/dsweber2/collatingTransform.jl)
[![Codecov](https://codecov.io/gh/dsweber2/collatingTransform.jl/branch/master/graph/badge.svg)](https://codecov.io/gh/dsweber2/collatingTransform.jl)
[![Coveralls](https://coveralls.io/repos/github/dsweber2/collatingTransform.jl/badge.svg?branch=master)](https://coveralls.io/github/dsweber2/collatingTransform.jl?branch=master)

# Notes on various implementation choices
## Pooling
	A stride of 1 is chosen so that rates appreciably close to 1 remain
    meaningful. A stride of 2 inherently subsamples at a rate of 2 +/- division
    issues. The net effect of max pooling in general is a reduction in total
    variation, with any gradients being slightly concentrated (as the
    subsampling rate approaches 1, we can expect an unusual number of entries
    to *exactly* match their neighbors). Pad is set to zero purely because of
    broken code, and doesn't make a real difference (effectively acts like a
    ReLU). Ideally it is set to k-1 so that the length remains fixed size, but
    this doesn't really effect the fidelity of the interpolation.
