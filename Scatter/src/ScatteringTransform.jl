# TODOs
# Chain that automatically calculates the necessary size when using collating
# transforms (both batch size and array size)

# 1D and 3D versions of internal, the random, and the pre-constructed

# actually calculate the Curvatures for the curvature examples

module ScatteringTransform

using ChainRules
using CUDA
using Flux:mse, update!
using FFTW
using Zygote, Flux, Shearlab, LinearAlgebra, AbstractFFTs
using Flux
using FourierFilterFlux
using Adapt
using RecipesBase
using Base: tail
using ChainRulesCore
using Plots # who cares about weight really?

import Adapt: adapt
import ChainRules:rrule
import Zygote:has_chain_rrule, rrule
export pad, poolSize, RationPool, nPoolDims, batchOff, originalDomain, params!, adapt,
    nFrame
# layer types
export ConvFFT, shearingLayer, waveletLayer, averagingLayer,
    scatteringTransform 
# scattering transform utils
export flatten, roll, normalize, scattered, pathLocs, getindex, getproperty,
    rrule, getWavelets, cat, nonZeroPaths, broadcast, broadcastable, normalize
# training utils
export makeObjFun, fitReverseSt, continueTrain, acc, plotPathResults, maximizeSingleCoordinate, chooseLargest, fitUsingOptim, fitByPerturbing
# inits
export positive_glorot_uniform, iden_perturbed_gaussian, uniform_perturbed_gaussian
export buildRecord, expandRecord!, addCurrent!
# interpretation tools
export ∇st, plotFirstLayer1D, gifFirstLayer, plotSecondLayer1D, addNextPath,plotSecondLayer
include("Util.jl")
include("scattering.jl")
include("trainingStorageTools.jl")
include("interpretationTools.jl")

import Flux.functor
function functor(cft::scatteringTransform{D, A, B, C, E, F}) where {D, A, B, C, E, F}
    return (cft.mainChain,), y->scatteringTransform{D, A, typeof(y[1]), 
                                 C, E, F}(cft.m, y[1], cft.normalize,
                                          cft.outputSizes, cft.outputPool,
                                          cft.settings)
end


import FourierFilterFlux.formatJLD
function formatJLD(st::scatteringTransform{D, A, B, C, E, F}) where {D, A, B, C, E, F}
    newChain = Chain(formatJLD(st.mainChain.layers)...)
    println("what the hey")
    scatteringTransform{D,A,typeof(newChain), 
                        C, E, F}(st.m, newChain, st.normalize, st.outputSizes,
                                 st.outputPool, st.settings)
end

end # module
