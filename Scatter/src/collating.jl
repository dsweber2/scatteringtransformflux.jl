########################################################################################
############################ collating transform #######################################
########################################################################################



# TODO: initialize the 1x1 convolution so that it is a block diagonal structure adding
# only those elements from the same frame/scale
# TODO change the 1×1 convolution so that it is norm 1 on initialization

"""
    collatingLayer(inputSize::Union{Int,NTuple{N, T}};
                        scale=4, shearLevel=scale, useGpu=true,
                        dType = Float32, σ=relu, plan=true,
                        trainable=false) where {N, T}
A collating layer consists of a ConvFFT layer and a 1x1 "convolutional" 
layer that sums across filters. There's also a reshaping function sandwiched
between the two.
"""
function collatingLayer(inputSize::Union{Int,NTuple{N, T}},
                        ch::Pair{<:Integer, <:Integer}; nConvDims=2,
                        σ=relu, init=uniform_perturbed_gaussian, useGpu=false, varargs...) where {N, T}
    @assert ch[1] == inputSize[nConvDims+1]
    if nConvDims==1
        filterLayer = waveletLayer(inputSize; σ=identity, useGpu=useGpu, varargs...)
    elseif nConvDims==2
        filterLayer= shearingLayer(inputSize; σ=identity, useGpu=useGpu,varargs...)
    else
        error("nConvDims should be either 1 or 2")
    end
    collator = convReduce(ch, nFrames(filterLayer), nConvDims, σ, init)
    if useGpu
        collator = collator |> gpu
    end
    return Chain(filterLayer, collator)
end


struct convReduce{D,N}
    cs::NTuple{N,<:Conv}
end
"""
fully connected layer across the non-convolution dimensions, excluding the
  very last. D gives the number of convolution dimensions, while N gives the
  input size (the dimension immediately after the conv dimension).
Example: for a 2D convolution, with a signal of `size (50,50,17,3,100)`, and `ch`
    is `3=>2`, N should be 17.
"""
function convReduce(ch, N, D, σ, init=uniform_perturbed_gaussian, OT=Float32)
    
    cs = map(i->genCollator(ch, σ, init, D, OT), (1:N...,))
    return convReduce{D, N}(cs)
end

import Base.show
function show(io::IO, c::convReduce{D,N}) where {D,N}
    sz = size(c.cs[1].weight)
    inSize = sz[D+1]=>sz[D+2]
    σ=c.cs[1].σ
    print(io, "convReduce{D=$D,cD=$N}($(inSize),σ=$σ)")
end

import Flux.functor
function functor(cR::convReduce{D, N}) where {D,N}
    return (cR.cs,), y->convReduce{D,N}(y[1])
end
# make params work
function Flux.trainable(cR::convReduce)
    cR.cs
end


function (c::convReduce{D,N})(x) where {D,N}
    cat([applySingle(c.cs[ii], x, ii) for ii= 1:N]..., dims= D+1)
end

function applySingle(c, x::AbstractArray{<:Any,5}, ii)
    ax = axes(x)
    clipped = x[ax[1:2]..., ii, ax[4:end]...]
    c(clipped)
end

function applySingle(c, x::AbstractArray{<:Any,4}, ii)
    ax = axes(x)
    clipped = x[ax[1], ii:ii, ax[3:end]...] # have to add a dead variable because
    # 1D convolution isn't supported
    res = c(clipped)
    reshape(res, (size(res,1), size(res)[3:end]...))
end

function genCollator(ch, σ, init::Function, D, OT)
    #if OT <: Real
    #return Conv((fill(1, max(D,2))...,), (ch[1]) => ch[2], σ, init=init)
#elseif OT <: Complex
        # have to make it by hand in the complex case
        weight = Complex{OT}.((1+0im)*init(fill(1, max(D, 2))...,ch[1], ch[2]))
        return Conv(weight, (1+0im)*zeros(eltype(weight), ch[2]), σ)
#else
    #     error("why is the output not either real or complex? $OT")
    # end
end
