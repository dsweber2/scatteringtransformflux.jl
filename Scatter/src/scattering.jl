struct scatteringTransform{D, A, B, C, E, F} # D is the dimension
    m::A
    mainChain::B
    normalize::Bool
    outputSizes::C
    outputPool::E
    settings::F
end

import Base.ndims
ndims(s::scatteringTransform{D}) where D = D
nPathDims(ii) = 1+max(min(ii-2,1),0) # the number of path dimensions at layer ii (zeroth
# is ii=1)

function Base.show(io::IO, st::scatteringTransform)
    layers = st.mainChain.layers
    σ = layers[1].σ
    Nd = ndims(st)
    nFilters = [size(layers[i].weight,3)-1 for i=1:3:(3*st.m)]
    batchSize = getBatchSize(layers[1])
    print(io, "scatteringTransform{$(st.m), Nd=$(Nd), filters=$(nFilters), σ = " *
          "$(σ), batchSize = $(batchSize), normalize = $(st.normalize)}")
end

"""
A simple wrapper for the results of the scattering transform. Its one field
`result` contains a tuple of the results from each layer, in the order zero,
one, two. Say we have an example `s`. You can access layer i by `s[i]`, so
`s[0]` gives the zeroth layer
"""
struct scattered
    result
end

import Base.+, Base.-, Base.broadcast, Base.length, Base.size, Base.iterate, Base.broadcastable
+(a::scattered,b::scattered) = scattered(map((x,y)->x+y, a.result, b.result))
-(a::scattered,b::scattered) = scattered(map((x,y)->x-y, a.result, b.result))
broadcast(f, a::scattered, varargs...) = scattered(map(x->broadcast(f,x,varargs...), a.result))
broadcastable(a::scattered) = a.result
#broadcasted(f, a::scattered, varargs...) = broadcastedscattered(map(x->broadcast(f,x,varargs...), a.result))
iterate(a::scattered) = iterate(a.result)
iterate(a::scattered, b) = iterate(a.result, b)
length(sct::scattered) = length(sct.result)-1
ndims(sct::scattered) = ndims(sct.result[1])-2
size(sct::scattered) = map(x->size(x), sct.result)
import Base.similar
similar(sct::scattered) = scattered(map(x->similar(x), sct.result))
import Statistics.mean
function mean(a::scattered)
    scattered(map(x->mean(x,dims=ndims(x)), a.result))
end
import Base.cat
function cat(sc::scattered, sc1::scattered, sc2::Vararg{scattered, N}) where N
    results = map(x->x.result, (sc, sc1, sc2...))
    result = map(x->cat(x...,dims=ndims(x[1])), zip(results...))
    return scattered(tuple(result...))
end

Flux.@functor scattered
Zygote.@adjoint function scattered(result) 
    function ∇scattered(Δ)
        return (Δ.result,)
    end
    scattered(result), ∇scattered
end
accessType = Union{Colon, <:Integer, <:AbstractArray{<:Union{Integer,Bool}}, Nothing}
struct pathLocs{m}
    indices::Tuple{Vararg{<:Union{Tuple{Vararg{<:accessType, 3}}, 
                                  Tuple{Vararg{<:accessType, 4}},
                                  Tuple{Vararg{<:accessType, 5}},
                                  Nothing, BitArray}, m}}
end
# specifying on
function pathLocs(varargs...; m::Int=2, d::Int=1, exs=Colon())
    if length(varargs)==0
        paired = [(i,:) for i=0:m]
    else
        # pair each layer with the tuple
        paired = [(varargs[i], varargs[i+1]) for i=1:2:length(varargs)]
    end
    present = [x[1] for x in paired]
    notPresent = [ii for ii=0:m if !(ii in present)] # unspecified layers
    paired = [paired..., [(x,nothing) for x in notPresent]...]
    paired = sort(paired, by=(x)->x[1])
    indices = map(x->parseOne(x,d,exs), paired)
    fullList = map(ii->paired[ii+1], 0:m)
    return pathLocs{m+1}((indices...,))
end

function parseOne(x,d, exs)
    lay, select = x
    if select==nothing
        return nothing
    end
    if lay==0
        if select==Colon()
            return (map(x->Colon(),1:d)..., Colon(), exs)
        elseif length(select) == d+2
            return select
        elseif typeof(select) <: AbstractArray && ndims(select) == d+2
            return select
        elseif typeof(select) <: AbstractArray 
            return (select, 1, exs)
        else
            return (select..., 1, exs)
        end
    elseif lay==1
        if select==Colon()
            return (map(x->Colon(),1:d)..., Colon(), exs)
        elseif length(select) == d+2
            return select
        elseif typeof(select) <: AbstractArray && ndims(select) == d+2
            return select
        elseif typeof(select) <: AbstractArray || typeof(select)<:Number
            return (map(x->Colon(),1:d)..., select, exs)
        elseif length(select) == 1
            return (map(x->Colon(),1:d)..., select[1], exs)
        else
            return (select..., exs)
        end
    elseif lay>=2
        if select==Colon()
            return (map(x->Colon(),1:d)..., Colon(), Colon(), exs)
        elseif length(select) == d+3
            return select
        elseif typeof(select) <: AbstractArray && ndims(select) == d+3
            return select
        elseif typeof(select) <: AbstractArray # only specify the first layer
            return (map(x->Colon(),1:d+1)..., select, exs)
        elseif length(select) == 2 # specify both
            return (map(x->Colon(),1:d)..., select..., exs)
        else
            return (select..., 1, exs)
        end
    end
end
function pathLocs(s::scattered)
    return pathLocs{length(s.result)}(map(x->axes(x), s.result))
end
function pathLocs(ii, s::scattered)
    return pathLocs{length(s.result)}(map(x->axes(x), s.result))
end

import Base:getindex,show,setindex!
function Base.show(io::IO, s::scattered)
    print(io, "scattered{$(length(s.result)-1),$(typeof(s.result[1]))}")
    sizes = map(x->size(x),s.result)
    for x in sizes
        print(io,"\n    $(x)")
    end
end
Base.getindex(X::scattered, i::AbstractArray) = X.result[i .+ 1]
Base.getindex(X::scattered, i::Integer) = X.result[i+1]
function Base.getindex(X::scattered, c::Colon, i::Union{<:AbstractArray, <:Integer})
    scattered(map(x-> x[axes(x)[1:end-1]..., i], X.result))
end
function Base.getindex(X::scattered, p::pathLocs{m}) where m
    ijk = p.indices
    λ(ii, access) = X[ii][access...]
    λ(ii, access::Nothing) = nothing
    λ(ii, access::AbstractArray) = X[ii][access]
    res = Zygote.hook(x->demofun(x,"WHY NO GET INDEX"), filter(x->x!=nothing, map(λ, (0:length(ijk)-1), ijk)))
    if length(res) == 1
        return res[1]
    else
        return res
    end
end

function size(p::pathLocs)
    ł(x::Nothing) = Tuple{Vararg{<:Integer, 0}}()
    ł(x::AbstractArray) = size(x)
    # all that's left is a tuple of things
    ł(x) = map(a -> length(a), x)
    map(ł, p.indices)
end

###########################################################################
######### here begins the wall of adjoints #########
###########################################################################

# acc(s::scattered) = s.result
# Zygote.@adjoint acc(s::scattered) = s.result, Δ->(scattered(Δ),)
# function rrule(::typeof(getproperty), F::T, x::Symbol) where T <: scattered
#     println("probably not called")
#     function gp_sc(Ȳ)
#         println("this is being called")
#         ∂F = Composite{T}(Ȳ)
#         return NO_FIELDS, ∂F, DoesNotExist()
#     end
#     return getproperty(F, x), gp_sc
# end

# Zygote.@adjoint function getproperty(s::scattered, p::Symbol)
#     function λ(Δ)
#         println(Δ)
#         (scattered(Δ[1]),)
#     end
#     return getproperty(s.p), λ
# end

# this version is preferred but getting wrapped in an extra layer
# function rrule(::typeof(getindex), F::T, i::Integer) where T <: scattered
#     println("probably not called")
#     function getInd_rrule(Ȳ)
#         zeroNonRefed = map(ii-> ii==i ? Ȳ : Zero(), (1:length(F.result)...,))
#         ∂F = Composite{T}(zeroNonRefed)
#         println("this is being called $(∂F)")
#         return NO_FIELDS, ∂F, DoesNotExist()
#     end
#     return getindex(F, i), getInd_rrule
# end
Zygote.@adjoint function getindex(F::T, i::Integer) where T <: scattered
    function getInd_rrule(Ȳ)
        zeroNonRefed = map(ii-> ii-1==i ? Ȳ[1] : zeros(eltype(F.result[ii]),
                                                  size(F.result[ii])...),
                           (1:length(F.result)...,)) 
        ∂F = T(zeroNonRefed)
        return ∂F, nothing
    end
    return getindex(F, i), getInd_rrule
end

Zygote.@adjoint function getindex(F::T, inds::AbstractArray) where T<: scattered
    function getInd_rrule(Ȳ)
        zeroNonRefed = map(ii-> ii-1 in inds ? Ȳ[indexin(ii-1,inds)[1]] :
                           zeros(eltype(F.result[ii]), size(F.result[ii])...),
                           (1:length(F.result)...,)) 
        ∂F = T(zeroNonRefed)
        return ∂F, nothing
    end
    return getindex(F, inds), getInd_rrule
end

Zygote.@adjoint function getindex(x::T, p::pathLocs) where T <: scattered
    function getInd_rrule(Δ)
        zeroNonRefed = map(ii-> zeros(eltype(x.result[ii]),
                                      size(x.result[ii])...),
                           (1:length(x.result)...,))
        ∂x = T(zeroNonRefed)
        ∂x[p] = Δ
        return ∂x, nothing
    end
    return getindex(x,p), getInd_rrule
end

function Base.setindex!(X::scattered, v::Number, p::pathLocs{m}) where m
    res = X.result
    ijk = p.indices
    for (mm,ind) in enumerate(ijk)
        if typeof(ind) <: BitArray
            res[mm][ind] .= v
        elseif typeof(ind) <: Tuple{Vararg{<:Integer}}
            res[mm][ind...] = v
        elseif ind!=nothing
            res[mm][ind...] .= v
        end
    end
end
function Base.setindex!(X::scattered, v, p::pathLocs{m}) where m
    res = X.result
    ijk = p.indices
    inputInd = 1
    for (mm,ind) in enumerate(ijk)
        if typeof(ind) <: Tuple{Vararg{<:Integer}}
            res[mm][ind...] = v[inputInd]
            inputInd+=1
        elseif typeof(ind) <:Union{BitArray, Array{Bool}}
            netSize = count(ind)
            res[mm][ind] = v[inputInd:inputInd+netSize-1]
            inputInd+=netSize
        elseif  ind!=nothing
            res[mm][ind...] .= v[inputInd]
            inputInd+=1
        end
    end
end


import Base.union, Base.cat
union(a, b::Colon) = Colon()
union(a::Colon, b) = Colon()
union(a::Colon, b::Colon) = Colon()
function cat(ps::Vararg{pathLocs, N}) where N
    ł(a::Nothing,b::Nothing)=nothing
    ł(a::Nothing,b) = b
    ł(a,b::Nothing) = a
    ł(a,b) = union.(a,b)
    reduce((p1,p2)->pathLocs(map(ł, p1.indices, p2.indices)), ps)
end

"""
    paths = nonZeroPaths(sc; wholePath=true, allTogetherInOne=false)
given a scattered, return the pathLocs where the scattered is nonzero. `wholePath` is true if it returns the whole path, and not just the specific location in the signal. For example, if only `sc(pathLocs(1,(30,2)))` is nonzero, if `wholePath` is true, then `pathLocs(1,(2,))` will be returned while if it is false, `pathLocs(1,(30,2))` will be returned instead.
if `allTogetherInOne` is false, then each location is returned separately, otherwise they are joined into a single `pathLocs`.
"""
function nonZeroPaths(sc; wholePath=true, allTogetherInOne=false)
    if wholePath
        return wholeNonzeroPaths(sc,allTogetherInOne)
    else
        return partNonzeroPaths(sc,allTogetherInOne)
    end
end

function partNonzeroPaths(sc, allTogetherInOne)
    if !allTogetherInOne
        error("wholePath false and each path separate not currently implemented because it makes the backend gross")
end
    sz = size(sc)
    paths = Tuple{Vararg{pathLocs,0}}()
    Nd = ndims(sc)
    # zeroth layer
    nonZeroLocs = abs.(sc[0]) .> 0
    typeof(nonZeroLocs)
    if any(nonZeroLocs)
        paths = (paths..., pathLocs(0, nonZeroLocs,d=Nd))
    end
    # first layer
    nonZero1 = map(i->abs.(sc[pathLocs(1,i,d=Nd)]) .>0, 1:sz[2][end-1])
    nonZero1 = map(x->reshape(x, (size(x)[1:Nd]..., 1, size(x)[end])), nonZero1)
    nonZero1 = cat(nonZero1..., dims = Nd+1)
    if any(nonZero1)
        paths = (paths..., pathLocs(1,nonZero1, d = Nd))
    end
    # second layer
    nonZero2 = abs.(sc[pathLocs(2,:)]) .>0
    if any(nonZero2)
        paths = (paths..., pathLocs(2,nonZero2, d=Nd))
    end
    return cat(paths...)
end

function wholeNonzeroPaths(sc, allTogetherInOne=false)
    sz = size(sc)
    paths = Tuple{Vararg{pathLocs,0}}()
    # zeroth layer
    if maximum(sc[0] .>0)
        paths = (paths..., pathLocs(0, :))
    end
    # first layer
    nonZero1 = filter(i->maximum(abs.(sc[pathLocs(1,i)]))>0, 1:sz[2][end-1])
    paths = (paths..., map(x->pathLocs(1, x), nonZero1)...)
    
    # second layer
    nonZero2 = [pathLocs(2, (i,j)) for i=1:sz[3][end-2], j=1:sz[3][end-1] if maximum(abs.(sc[pathLocs(2,(i,j))]))>0]
    paths = (paths..., nonZero2...)
    if allTogetherInOne
        return cat(paths...)
    else
        return paths
    end
end



######################################################################## 
######################## Guts of the transform ######################### 
######################################################################## 



function checkDefault(name, names, varargs, ii)
    if names in keys(varargs) && typeof(varargs[names]) <: Union{<:Rational, <:Integer}
        return name => varargs[names]
    elseif names in keys(varargs)
        return name => varargs[names][ii]
    else
        return
    end
end

function checkDefaults(namePairs, varargs,ii)
    defaults = map(nam->checkDefault(nam...,varargs,ii), namePairs)
    return filter(x->x!=nothing, defaults)
end
function removeSTargs(varargs)
    listOfSTargs = [:scales, :shearLevels, :cws, :averagingLengths,
                    :decreasings]
    return filter(x->!(x[1] in listOfSTargs), varargs)
end

function dispatchLayer(listOfSizes, ii, Nd::Val{1}; varargs...)
    waveletLayer(listOfSizes[ii]; 
                 checkDefaults((:cw=>(:cws),
                                :averagingLength=>(:averagingLengths),
                                :decreasing=>(:decreasings), 
                                :s=>(:scales)),
                               varargs, ii)..., 
                 removeSTargs(varargs)...)
end


function dispatchLayer(listOfSizes, ii, Nd::Val{2}; varargs...)
    shearingLayer(listOfSizes[ii];
                  checkDefaults((:scale=>(:scales), 
                                 :shearLevel=>(:shearLevels)),
                                varargs, ii)...,
                  removeSTargs(varargs)...)
end

"""
    scatteringTransform(inputSize::NTuple{N}, m; trainable = false,
                        normalize = true, outputPool = 2,
                        poolBy= 3//2, σ=abs, scales=(8,8,8),
                        shearLevels=scales/2, cws=WT.morl,
                        averagingLengths=[4,4,4], 
                        decreasings=[2,2,2]) where {N}
    scatteringTransform(inputSize::NTuple{N}, m; trainable = false,
                        normalize = true, outputPool = 2,
                        poolBy= 3//2, σ=abs, scales=(4,4,4),
                        shearLevels=scales) where {N} 

Create a scattering transform of depth m (which returns a m+1 depth array of
arrays) that subsamples at a rate of poolBy each layer, using scales[i] and
shearLevels[i] at each layer. Normalize means give each layer the same average
weight per path, e.g. since the zeroth layer has one path, give it norm 1, if
the first layer has 16 paths, give it norm 16, etc. This is primarily for
cases where the classification algorithm needs roughly the same order of
magnitude variance.
"""
function scatteringTransform(inputSize::NTuple{N}, m; trainable = false,
                             normalize = true, outputPool = 2,
                             poolBy= 3//2, σ=abs, kwargs...) where {N} 
    # N determines the number of spatial dimensions
    Nd = N - 2;
    if typeof(outputPool) <: Union{<:Rational, <:Integer}
        outputPool = (ntuple(i->outputPool, Nd)...,)
    end
    if typeof(poolBy) <: Union{<:Rational, <:Integer}
        poolBy = [ RationPool((ntuple(i->poolBy, N-2)...,), nExtraDims = nPathDims(ii+1)+1) for ii=1:m+1]
    end

    listOfSizes = [(inputSize..., ntuple(i->1,max(i-1, 0))...) for i=0:m]
    interstitial = Array{Any,1}(undef, 3*(m+1)-2)
    for i=1:m
        # first transform
        interstitial[3*i-2] = dispatchLayer(listOfSizes, i, Val(Nd); σ=identity,
                                            kwargs...)
        nFilters = size(interstitial[3*i - 2].weight)[end]
        
        pooledSize = poolSize(poolBy[i], listOfSizes[i][1:Nd])
        # then throw away the averaging (we'll pick it up in the actual transform)
        # also, in the first layer, merge the channels into the first shearing,
        # since max pool isn't defined for arbitrary arrays
        if i==1
            listOfSizes[i+1] = (pooledSize...,
                                (nFilters-1)*listOfSizes[1][Nd+1],
                                listOfSizes[i][Nd+2:end]...)
            interstitial[3*i-1] = x -> begin
                ax = axes(x)
                return σ.(reshape(x[ax[1:Nd]..., ax[Nd+1][1:end-1], ax[(Nd+2):end]...],
                               (ax[1:Nd]..., listOfSizes[i+1][Nd+1], ax[end])))
            end
        else
            listOfSizes[i+1] = (pooledSize..., (nFilters-1),
                                listOfSizes[i][Nd+1:end]...)
            interstitial[3*i-1] = x-> begin
                ax = axes(x)
                tmp = hook(x->demofun(x,"subset"), x[ax[1:Nd]..., ax[Nd+1][1:end-1], ax[Nd+2:end]...])
                tmp = hook(x->demofun(x,"pre broadcast"), tmp)
                tmp = σ.(tmp)
                tmp = hook(x->demofun(x,"post broadcast"), tmp)
                return tmp
            end
        end
        # then pool
        interstitial[3*i] = poolBy[i]
    end
    # final averaging layer
    interstitial[3*m+1] = dispatchLayer(listOfSizes, m+1, Val(Nd);
                                        averagingLayer=true, σ=identity, 
                                        kwargs...)
    chacha = Chain(interstitial...)
    outputSizes = ([(map(poolSingle, outputPool, x[1:Nd])..., x[(Nd+1):end]...) for x
                    in listOfSizes]...,)

    # record the settings used pretty kludgy
    settings = (:outputPool=>outputPool, :poolBy=>poolBy, :σ=>σ, kwargs...)
    return scatteringTransform{Nd, typeof(m), typeof(chacha),
                               typeof(outputSizes), typeof(outputPool),
                               typeof(settings)}(m, chacha, normalize,
                                                 outputSizes, outputPool,
                                                 settings)
end

"""
wave1, wave2, wave3, ... = getWavelets(sc::scatteringTransform)

just a simple util to get the wavelets from each layer
"""
function getWavelets(sc::scatteringTransform)
    map(x->x.weight, filter(x->(typeof(x) <: ConvFFT), sc.mainChain.layers)) # filter to only
    # have ConvFFTs, and then return the wavelets of those
end
"""
    result = flatten(scatRes)
given the result of a scattering transform, flatten it
"""
function flatten(scatRes)
    if typeof(scatRes) <: Union{CuArray{<:Any,2}, Array{<:Any,2},
                                Adjoint{<:Any, <:CuArray{<:Any,2}}}
        return scatRes
    end
    res = scatRes.result
    netSizes = [prod(size(r)[1:end-1]) for r in res]
    relevantLocs = [sum(netSizes[1:(i)]) for i in 0:length(netSizes)]
    batchSize = size(res[1])[end]
    singleExampleSize = sum(netSizes)
    #result = adapt(typeof(res[1]), zeros(singleExampleSize, batchSize))
    result = cat([reshape(x, (netSizes[i], batchSize)) for (i,x) in enumerate(res)]..., dims=1)
    # for (i,x) in enumerate(scatRes)
    #     indices = (1+relevantLocs[i]):relevantLocs[i+1]
    #     result[indices, :] = reshape(x, (netSizes[i], batchSize))
    # end
    return result
end



"""
    rolled = roll(toRoll, st)
Given the result of a scattering transform and something with the same number
of entries but in an array that is NCoeffs×extraDims, roll up the result
into an array of arrays like the scattered.
"""
function roll(toRoll, st::scatteringTransform)
    Nd = ndims(st)
    oS = st.outputSizes
    nExamples = size(toRoll)[2:end]
    rolled = ([adapt(typeof(toRoll), zeros(eltype(toRoll),
                                          sz[1:Nd+nPathDims(ii)]...,
                                          nExamples...)) for (ii,sz) in
              enumerate(oS)]...,);

    locSoFar = 0
    for (ii, x) in enumerate(rolled)
        szThisLayer = oS[ii][1:Nd+nPathDims(ii)]
        totalThisLayer = prod(szThisLayer)
        range = (locSoFar + 1):(locSoFar + totalThisLayer)
        addresses = (szThisLayer..., nExamples...)
        rolled[ii][:] = reshape(toRoll[range, :], addresses)
        locSoFar += totalThisLayer
    end
    return scattered(rolled)
end

"""
given a scattered result, make a list that gives the largest value on each path
"""
function importantCoords(scatRes)
    return [dropdims(maximum(abs.(x),dims=(1,2)), dims=(1,2)) for x in scatRes]
end


# actually apply the transform
function (st::scatteringTransform)(x::T) where {T<:AbstractArray}
    mc = st.mainChain.layers
    res = applyScattering(mc, x, ndims(st), st)
    return scattered(res)
end

function demofun(x,msg)
    #@show((typeof(x), typeof(x[1]), maximum(abs.(x)), size(x), msg))
    #@show((findfirst(isnan.(x)),count(isnan.(x))))
    return x
end
function mindemo(x,msg)
    #@show((typeof(x), msg))
    if length(x)>=1
        #@show(maximum(abs.(x[1])))
    end
    return x
end

function applyScattering(c::Tuple, x, Nd, st)
    res = Zygote.hook(x->demofun(x, "applying $(first(c))"), first(c)(x))
    if typeof(first(c)) <: ConvFFT
        res = Zygote.hook(x->demofun(x, "noop the second"), res)
        tmpRes = Zygote.hook(x->demofun(x, "join prev result"), res[map(x->Colon(), 1:Nd)..., end, map(x->Colon(), 1:ndims(res)-Nd-1)...])
        # return a subsampled version of the output at this layer
        poolSizes = (st.outputPool..., ntuple(i->1, ndims(tmpRes)-Nd-2)...)
        r = RationPool(st.outputPool, nExtraDims = ndims(tmpRes)-Nd)
        if st.normalize
            tmpRes = Zygote.hook(x->demofun(x, "noop"), tmpRes)
            tmpRes = Zygote.hook(x->demofun(x, "output $(length(c))"), normalize(r(real.(tmpRes)), Nd))
            apld = Zygote.hook(x->mindemo(x,"whaaaart"), applyScattering(tail(c), res, Nd, st))
            return (tmpRes,
                    apld...)
        else
            tmpRes = r(real.(tmpRes))
            return (tmpRes,
                    applyScattering(tail(c), res, Nd, st)...)
           end
    else
        # this is either a reshaping layer or a subsampling layer, so no output
        return applyScattering(tail(c), res, Nd, st)
    end
end

applyScattering(::Tuple{}, x, Nd, st) = tuple() # all of the returns should
# happen along the way, not at the end

"""
normalize x over the dimensions Nd through ndims(x)-1, e.g. for a Nd=2, and x
that is 4D, then norm(x[:,:,:,j], 2) ≈ size(x,3)
"""
function normalize(x, Nd)
    n = ndims(x)
    totalThisLayer = prod(size(x)[(Nd+1):(n-1)])
    ax = axes(x)
    buf = Zygote.Buffer(x)
    for i=1:size(x)[end]
        xSlice = x[ax[1:(n-1)]..., i]
        thisExample = totalThisLayer / (sum(abs.(xSlice).^2)).^(0.5f0)
        if isnan(thisExample) || thisExample≈Inf
            buf[ax[1:(n-1)]..., i] = xSlice
        else
            buf[ax[1:(n-1)]..., i] = xSlice .* thisExample
        end
    end
    return copy(buf)
end

normalize(sct::scattered, Nd) = scattered(map(x->normalize(x,Nd), sct.result))
    
maxpoolNd(x, poolSz) = maxpool(x,poolSz)
function maxpoolNd(x, poolSz::Tuple{<:Integer})
    shaped = reshape(x, (size(x,1), 1, size(x)[2:end]...))
    subsampled = maxpool(shaped, (poolSz..., 1))
    ax = axes(subsampled)
    return subsampled[ax[1], 1, ax[3:end]...]
end
