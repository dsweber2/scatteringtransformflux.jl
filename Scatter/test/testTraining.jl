using LinearAlgebra
@testset "auxiliary tools" begin
    using collatingTransform:genNoise
    x = genNoise((129,1,3), 503,:white);
    @test size(x) == (129,1,3,503)
    @test norm(x) ≈ 1
    eltype(x)==Float32
    x = genNoise((129,1,3), 503,:pink);
    @test size(x) == (129,1,3,503)
    @test norm(x) ≈ 1
    @test eltype(x)==Float32
end
