# TODOs
# test positive_glorot_uniform
# test bias
#using Revise
using collatingTransform
using Test
using Flux, CUDA, AbstractFFTs, LinearAlgebra
using FourierFilterFlux, Wavelets, Shearlab

ifGpu = identity
i=40; s =6//5
nExtraDims = 2
xExtraDims = 3
k = 4; N=2
import collatingTransform:stopAtExactly_WithRate_
@testset "testing pooling" begin
    subsampRates = [3//2, 2, 5//2, 6//5]
    windowSize=[2,3,4]
    NdimsExtraDims = [(N,extra) for extra in 2:4, N in 1:2 if !(N==2 && extra==4)]
    @testset "bizzare pooling sizes 1D i=$i, s=$s, nExtraDims=$nExtraDims, xExtraDims=$xExtraDims, k=$k N=$N" for i=25:3:40, s in subsampRates, xExtraDims in 2:nExtraDims, k in windowSize, (N,nExtraDims) in NdimsExtraDims
        selectXDims = ntuple(x->2, xExtraDims)
        x = ifGpu(randn(ntuple(x->i, N)...,rand(2:10, xExtraDims)...)); size(x)
        r = RationPool(ntuple(x->s, N),k,nExtraDims=nExtraDims)
        @test length(r.m.k) == length(r.resSize)+nExtraDims-2
        nPoolDims(r)
        Nneed = ndims(r.m)+2
        Sx = r(x);
        @test size(Sx)==(poolSize(r, size(x))..., size(x)[N+1:end]...)
        # does it take the mean we expect?
        # size(x)
        # size(r.m(randn(25-k+2,8,5)))
        # 25-k+1
        # size(r.m(x))
        chosenLocs = stopAtExactly_WithRate_(i-k+1,s)
        loc = chosenLocs[5]
        neighborhood = 0:k-1#floor(Int64,(k+1)/2)
        neighborhood = ntuple(x->loc .+ neighborhood, N)
        # size(Sx)
        # size(x)
        # Sx[4,2,2]
        # mean(x[neighborhood...,selectXDims...])
        # mean(x[-2 .+ (5:8),selectXDims...])
        @test Sx[ntuple(x->5, N)...,selectXDims...] ≈ 
            mean(x[neighborhood...,selectXDims...])
        # which entries matter for the 5th entry, according to the gradient?
        ∇ = gradient(x->r(x)[ntuple(x->5, N)..., selectXDims...],x);
        δ = zeros(size(x)); δ[neighborhood..., selectXDims...] .= 1/k^N
        findall(∇[1].!=0)
        findall(δ.!=0)
        @test ∇[1] ≈ δ
    end
end

@testset "ConvReduce" begin
    function testConvReduce(inChan,outChan, nFrameEl, nDims, nonLin)
        cR = convReduce(inChan=>outChan,nFrameEl,nDims,nonLin)
        P = params(cR).order
        @test length(P)==2*nFrameEl
        @test minimum([size(P[ii])==(fill(1, 2)...,inChan,outChan) for ii=1:length(P) if ii%2==1])
        @test minimum([size(P[ii])==(outChan,) for ii=1:length(P) if ii%2==0])
        #@test typeof(gpu(cR).cs[1].weight) <:CuArray
        x = randn(fill(50,nDims)...,nFrameEl,inChan,1);
        res = cR(x)
        @test size(res)==(fill(50,nDims)...,outChan*nFrameEl,1)
        # todo add tests for complex input types
    end
    testConvReduce(3,2,17,2,identity)
    testConvReduce(3,2,17,1,identity)
end

@testset "Collating Transform 2D" begin
    nChannelsOut = 11
    nChannelsIn = 3
    startSize = (50,50)

    cL = collatingLayer((startSize..., nChannelsIn, 1),
                        nChannelsIn=>nChannelsOut,
                        σ=abs, init=iden_perturbed_gaussian,scale=4,
                        useGpu=false)
    L = length(params(cL).order)
    @test L==2*nFrames(cL[1])
    @test minimum([size(params(cL).order[ii]) == (1,1,nChannelsIn, nChannelsOut)
                   for ii=1:L if ii%2 ==1])
    @test minimum([size(params(cL).order[2]) == (nChannelsOut,) for ii=1:L if
                   ii%2 ==1])
    res = cL(randn(startSize...,nChannelsIn, 1));
    @test size(res) == (startSize..., 49*nChannelsOut, 1)

    # gpu version
    cL = collatingLayer((startSize..., nChannelsIn, 1),
                        nChannelsIn=>nChannelsOut,
                        σ=abs, init=iden_perturbed_gaussian,scale=4, useGpu=true)
    @test length(params(cL).order) == 2*nFrames(cL[1])
    @test size(params(cL).order[1]) == (1,1,nChannelsIn, nChannelsOut)
    @test size(params(cL).order[2]) == (nChannelsOut,)
    res = cL(ifGpu(randn(startSize...,nChannelsIn, 1)));
    @test size(res) == (startSize..., 49*nChannelsOut, 1)


    cL = collatingLayer((startSize..., nChannelsIn, 1),
                        nChannelsIn=>nChannelsOut,
                        σ=abs, init=iden_perturbed_gaussian,scale=4,
                        useGpu=false)

    # each group of 11 comes from a unique channel
    k = 4
    @time ∇ = gradient(()-> sum(cL(randn(startSize...,nChannelsIn, 1))[1,1,11*(k-1)+1:11*k,1]), params(cL));
    p = params(cL).order
    nonZero = fill(true,nFrames(cL[1])); nonZero[k]=false
    # only the kth entry is nonzero
    @test [∇.grads[p[ii]] ≈ zeros(1,1,3,11) for ii=1:length(p) if ii%2==1]==nonZero
    @test [∇.grads[p[ii]] ≈ zeros(11) for ii=1:length(p) if ii%2==0]==nonZero
end

@testset "Collating Transform 1D" begin
    nChannelsOut = 11
    nChannelsIn = 3
    startSize = 50

    cL = collatingLayer((startSize, nChannelsIn, 1),
                        nChannelsIn=>nChannelsOut,
                        σ=abs, init=iden_perturbed_gaussian,s=4,
                        useGpu=false, nConvDims=1)
    @test length(params(cL).order) == 2*nFrames(cL[1])
    @test size(params(cL).order[1]) == (1,1,nChannelsIn, nChannelsOut)
    @test size(params(cL).order[2]) == (nChannelsOut,)
    res = cL(randn(startSize...,nChannelsIn, 1));
    @test size(res) == (startSize..., 6*nChannelsOut, 1)

    cL = collatingLayer((startSize, nChannelsIn, 1),
                        nChannelsIn=>nChannelsOut,
                        σ=abs, init=iden_perturbed_gaussian,s=4, 
                        nConvDims=1)
    # TODO: gpu version is broken, but this is changing soon

    cL = cL |> ifGpu
    typeof(cL.layers[1].fftPlan)
    @test length(params(cL).order) == 2*nFrames(cL[1])
    @test size(params(cL).order[1]) == (1,1,nChannelsIn, nChannelsOut)
    @test size(params(cL).order[2]) == (nChannelsOut,)
    res = cL(ifGpu(randn(startSize..., nChannelsIn, 1)));
    @test size(res) == (startSize..., 6*nChannelsOut, 1)

    # each group of 11 comes from a unique channel
    k = 3
    @time ∇ = gradient(()-> sum(cL(randn(startSize...,nChannelsIn, 1))[1,11*(k-1)+1:11*k,1]), params(cL));
    p = params(cL).order
    nonZero = fill(true,nFrames(cL[1])); nonZero[k]=false
    nonZero
    # only the kth entry is nonzero
    ∇.grads[p[1]]
    @test [∇.grads[p[ii]] ≈ zeros(1,1,3,11) for ii=1:length(p) if ii%2==1]==nonZero
    @test [∇.grads[p[ii]] ≈ zeros(11) for ii=1:length(p) if ii%2==0]==nonZero
    # TODO: fairly certain these are broken for reasons related to NNLib disfunction
end

@testset "Scattering Transform" begin
    @testset "Utils" begin
        @test collatingTransform.nPathDims(1)==1
        @test collatingTransform.nPathDims(2)==1
        @test collatingTransform.nPathDims(3)==2
        function wrapVarargs(; varargs...)
            return varargs
        end
        va = wrapVarargs(scales=8,shearLevels=5,cws=[WT.morl, WT.Morlet(π)])
        @test length(collatingTransform.removeSTargs(va))==0
        # 1D args
        inputSize=(100,3); m=2
        listOfSizes = [(inputSize..., fill(1,max(i-1, 0))...) for i=0:m]
        oneDArgs = wrapVarargs(scales=8,cws=[WT.morl, WT.Morlet(3π),
                                             WT.dog2],averagingLengths=[2,4,6],
                               decreasing=2)
        resArgs = collatingTransform.checkDefaults((:cw=>(:cws),
                                                    :averagingLength=>(:averagingLengths),
                                                    :decreasing=>(:decreasings)),
                                                   oneDArgs,1)
        @test resArgs==((:cw=>WT.morl), (:averagingLength=>2))
        # 2D args
        inputSize=(50,50,3); m=2
        listOfSizes = [(inputSize..., fill(1,max(i-1, 0))...) for i=0:m]
        twoDArgs = wrapVarargs(scales=3)
        resArgs = collatingTransform.checkDefaults((:scale=>(:scales),
                                                    :shearLevel=>(:shearLevels)),
                                                   twoDArgs,1)
        @test resArgs==(:scale =>3,)
    end
    @testset "1D layer methods" begin
        inputSize=(100,3); m=2
        listOfSizes = [(inputSize..., fill(1,max(i-1, 0))...) for i=0:m]
        oneDArgs = wrapVarargs(scales=8,cws=[WT.morl, WT.Morlet(3π),
                                             WT.dog2],averagingLengths=[2,4,6],
                               decreasing=2)

        # layer 1
        lay = collatingTransform.dispatchLayer(listOfSizes, 1, Val(1);
                                               oneDArgs...)
        Ψ = wavelet(WT.morl,s=8,averagingLength=2,decreasing=2)
        waves,ω = computeWavelets(inputSize, Ψ);
        @test cpu(lay.weight) ≈ cat(waves[:, 2:end],waves[:,1],dims=2)
        # layer 2
        lay = collatingTransform.dispatchLayer(listOfSizes, 2, Val(1); oneDArgs...)
        Ψ = wavelet(WT.Morlet(3π),s=8,averagingLength=4,decreasing=2)
        waves,ω = computeWavelets(inputSize, Ψ);
        @test cpu(lay.weight) ≈ cat(waves[:, 2:end],waves[:,1],dims=2)

        # layer 3
        lay = collatingTransform.dispatchLayer(listOfSizes, 3, Val(1);
                                               averagingLayer=true, oneDArgs...)
        Ψ = wavelet(WT.dog2,s=8,averagingLength=6,decreasing=2)
        waves,ω = computeWavelets(inputSize, Ψ);
        size(waves)
        @test cpu(lay.weight) ≈ waves[:, 1]
    end

    @testset "2D tests" begin
        inputSize=(50,50,3); m=2
        listOfSizes = [(inputSize..., fill(1,max(i-1, 0))...) for i=0:m]
        twoDArgs = wrapVarargs(scales=3)
        # layer 1
        lay = collatingTransform.dispatchLayer(listOfSizes, 1, Val(2);
                                               twoDArgs...)
        shears = Shearlab.getshearletsystem2D(inputSize[1:2]..., 3);
        @test cpu(lay.weight) ≈ shears.shearlets
    end
end

@testset "2D basics" begin
    init = ifGpu(10 .+ randn(32,32, 3, 2));
    sst = scatteringTransform(size(init), 2, poolBy=3//2)
    res = sst(init);
    @test length(res.result)== 2+1
    @test size(res.result[1]) == (16, 16, 3, 2)
    @test minimum(abs.(res.result[1])) > 0
    @test size(res.result[2]) == (11, 11, 144, 2)
    @test minimum(abs.(res.result[2])) > 0
    @test size(res.result[3]) == (7, 7, 16, 144, 2)
    @test minimum(abs.(res.result[3])) > 0
    totalSize = 16*16*3 + 10*10*144 + 7*7*16*144
    smooshed = collatingTransform.flatten(res);
    @test size(smooshed) ==(totalSize, 2)
end

@testset "1D basics" begin
    init = ifGpu(10 .+ randn(64, 3, 2));
    sst = scatteringTransform(size(init), 2, poolBy=3//2, outputPool=(2,))
    res = sst(init)
    @test length(res.result)== 2+1
    @test size(res.result[1]) == (32, 3, 2)
    @test minimum(abs.(res.result[1])) > 0
    @test size(res.result[2]) == (22, 3*11, 2)
    @test minimum(abs.(res.result[2])) > 0
    @test size(res.result[3]) == (14, 8, 3*11, 2)
    @test minimum(abs.(res.result[3])) > 0
    totalSize = 32*3 + 22*3*11 + 14*8*3*11
    smooshed = collatingTransform.flatten(res);
    @test size(smooshed) == (totalSize, 2)
end

# integer pooling rate
@testset "2D integer pooling" begin
    st = scatteringTransform((131,131,1,1), 2, poolBy=3)

    scat = st(ifGpu(randn(131,131,1,1)))
    @test size(st.mainChain[1].fftPlan) == (391, 391, 1, 1)
    @test size(st.mainChain[4].fftPlan) == (96, 106, 48, 1)
    @test size(st.mainChain[7].fftPlan) == (43,43,48,48,1)
    @test st.mainChain[1].bc.padBy == (130,130)
    @test st.mainChain[4].bc.padBy == (26, 31)
    @test st.mainChain[7].bc.padBy == (14, 14)
    @test st.outputPool == (2,2)
    @test ndims(st)==2
    resultSize = ((66, 66, 1, 1), (22, 22, 48, 1), (8, 8, 48, 48, 1))
    @test st.outputSizes == resultSize
    @test ([size(s) for s in scat.result]...,) == resultSize
end

@testset "1D integer pooling" begin
    st = scatteringTransform((131,1,1), 2, poolBy=3)

    scat = st(ifGpu(randn(131,1,1)));
    @test size(st.mainChain[1].fftPlan[1]) == (2*131,1,1)
    @test size(st.mainChain[1].fftPlan[2]) == (2*131,1,1)
    @test size(st.mainChain[4].fftPlan[1]) ==(2*44, 14,1)
    @test size(st.mainChain[4].fftPlan[2]) ==(2*44, 14,1)
    @test size(st.mainChain[7].fftPlan[1]) == (2*15,8,14,1)
    @test size(st.mainChain[7].fftPlan[2]) == (2*15,8,14,1)
    @test st.mainChain[1].bc == Sym()
    @test st.mainChain[4].bc == Sym()
    @test st.mainChain[7].bc == Sym()
    @test st.outputPool == (2,)
    @test ndims(st)==1

    resultSize = ((66, 1, 1), (22, 14, 1), (8, 8, 14, 1))
    @test st.outputSizes == resultSize
    @test ([size(s) for s in scat.result]...,) == resultSize
end

# rolling and flattening does nothing
@testset "roll and flatten 2D" begin
    init = ifGpu(randn(32,32, 1, 2));
    sst = scatteringTransform(size(init), 2, poolBy=3//2)
    res = sst(init);
    smooshed = collatingTransform.flatten(res);
    if ifGpu!= identity
        @test typeof(smooshed) <: CuArray
    end

    reconst = roll(smooshed, sst);
    for ii=1:length(reconst)
        @test reconst[ii]≈res.result[ii]
    end
    @test typeof(reconst.result) <: Tuple
end

@testset "roll and flatten 1D" begin
    init = ifGpu(randn(64, 1, 2));
    sst = scatteringTransform(size(init), 2, poolBy=3//2)
    res = sst(init);
    smooshed = collatingTransform.flatten(res);
    if ifGpu!= identity
        @test typeof(smooshed) <: CuArray
    end

    reconst = roll(smooshed, sst);
    for ii=1:length(reconst)
        @test reconst[ii]≈res.result[ii]
    end
    @test typeof(reconst.result) <: Tuple
end

# normalization in 2D
x = randn(10,4,3,5,7);
Nd=2;
xp = collatingTransform.normalize(x,2);
for w in eachslice(xp,dims=ndims(x))
    @test norm(w,2) ≈ 3*5
end

# normalization in 1D
x = randn(10,3,5,7);
xp = collatingTransform.normalize(x,1);
for w in eachslice(xp,dims=ndims(x))
    @test norm(w,2) ≈ 3*5
end
end
@testset "pathLocs" begin
    using collatingTransform:parseOne
    # layer 0
    @test (34:53, 1, 1)==parseOne((0,34:53),1)
    @test (34:53, 34:53, 1, 1)==parseOne((0,(34:53, 34:53)),2)
    @test (:,:,:)==parseOne((0,:),1)
    @test (:,:,:,:)==parseOne((0,:),2)
    @test (4:35,1,1:10) == parseOne((0,(4:35,1,1:10)),1)
    @test (4:35,1,1,1:10) == parseOne((0,(4:35,1,1,1:10)),2)
    # layer 1
    @test (:, 34:53, :) == parseOne((1,34:53),1)
    @test (:, 5, :) == parseOne((1,5),1)
    @test (:, :, 34:53, :)==parseOne((1,34:53),2)
    @test (:,:, 5, :) == parseOne((1,5),2)
    @test (:,:,:)==parseOne((1,:),1)
    @test (:,:,:,:)==parseOne((1,:),2)
    @test (4:35,1,1:10) == parseOne((1,(4:35,1,1:10)),1)
    @test (4:35,1,1,1:10) == parseOne((1,(4:35,1,1,1:10)),2)
    # layer 2
    @test (:, :, 34:53, :) == parseOne((2,34:53),1)
    @test (:, :, :, 34:53, :)==parseOne((2,34:53),2)
    @test (:, 34:53, 5, :) == parseOne((2,(34:53,5)),1)
    @test (:, :, 34:53, 5, :)==parseOne((2,(34:53,5)),2)
    @test (:,:,:,:)==parseOne((2,:),1)
    @test (:,:,:,:,:)==parseOne((2,:),2)
    @test (4:35,1,1:10,1,1) == parseOne((2,(4:35,1,1:10)),1)
    @test (4:35,1,1,1:10,1,1) == parseOne((2,(4:35,1,1,1:10)),2)

    # actual pathLocs construction
    @test ((:,:,:),(:,3:4,:), (:,5,:,:))==pathLocs(1,3:4, 0,:, 2,(5,:)).indices
    @test ((:,:,:),nothing, (:,5,:,:))==pathLocs(0,:, 2,(5,:)).indices

    # pathLocs usage
    ex = scattered((randn(50,1,1), randn(34,13,1), randn(23,11,13,1)))
    p = pathLocs(0,:, 2,(5,:))
    @test minimum((ex.result[1], ex.result[3][:,5,:,:]) .≈ ex[p])
    p = pathLocs(2, (4:9, 5))
    @test ex.result[3][:,4:9,5,:]≈ ex[p]
    p = pathLocs(1,:)
    @test ex.result[2] ≈ ex[p]
    p = pathLocs()
    @test minimum(ex[p] .≈ ex.result)

    # setindex (some more in the nonZero paths section)
    p = pathLocs(2, (4:9, 5))
    newVal = randn(23,6,1) 
    ex[p] = newVal
    @test ex[p] ≈ newVal
    
    p = pathLocs(2, (4:9, 5), 1, (3:5,))
    newVal = (randn(34,3,1), randn(23,6,1))
    ex[p] = newVal
    @test all(ex[p] .≈ newVal)

    # this is still broken; some sort of broadcast shenanigans
    p = pathLocs(0,40:46)
    ex[p] .= 1
    @test minimum(ex[p] .≈ 1)

    # single entries behave a bit strangely
    p = pathLocs(0, 3)
    newVal = randn()
    ex[p] = newVal
    @test minimum(ex[p] .≈ newVal)
    

    # getindex using ints and arrays
    @test ex[0][1] == ex.result[1]
    @test ex[1][1] == ex.result[2]
    @test ex[0:1] == ex.result[1:2]


    # pathLoc getindex adjoint
    p = pathLocs(2, (4:9, 5))
    y, back = pullback(x->x[p], ex)
    res = back(y)
    @test res[1][p] == ex[p]
    anti_p = pathLocs(2, ([1:3..., 10,11], [1:4... 6:13...]))
    @test res[1][anti_p][1] ==zeros(23,5,1,12,1)
    @test res[1][0:1]==(zeros(size(ex[0][1])...), zeros(size(ex[1][1])...))
    

    y, back = pullback(getindex, ex, 1);
    res = back(y);
    @test res[1][1] == ex[1]
    @test res[1][0][1] == zeros(size(ex[0][1])...)
    @test res[1][2][1] == zeros(size(ex[2][1])...)
    @test res[2]==nothing

    y, back = pullback(getindex, ex, 1:2);
    res = back(y);
    @test res[1][1:2] == ex[1:2]
    @test res[1][0][1] == zeros(size(ex[0][1])...)
    @test res[2]==nothing

    # catting paths (may result in extra indices grabbed)
    pathsByHand = (pathLocs(0,40:46,exs=1), pathLocs(1,(24:26,4),exs=2),
                   pathLocs(1,(29:31,6),exs=1:2),
                   pathLocs(2,(11:14,10,1), exs=1), 
                   pathLocs(2, (11:15, 3, 5:7), exs=1))
    joint = cat(pathsByHand...)
    #(24:26,4,2) join (29:32,6,1:2) = (3,1,1) and (4,1,2)
    
    # nonZeroPaths
    ex = scattered((zeros(50,1,2), zeros(34,13,2), zeros(23,11,13,2)))
    for (ii, p) in enumerate(pathsByHand)
        ex[p] = (-1)^ii * ii * ones(size(ex[p]))
    end
    paths = nonZeroPaths(ex,wholePath=false, allTogetherInOne=true)
    @test ex[paths][1] == ex[pathLocs(0,40:46,exs=1)]
    @test ex[paths][2] == [fill(-3.0,3)...; fill(2.0,3)...; fill(-3.0,3)...] #not
    #easy to describe the location
    @test findall(paths.indices[2]) == [CartesianIndex(29, 6, 1),CartesianIndex(30, 6, 1),CartesianIndex(31, 6, 1),CartesianIndex(24, 4, 2),CartesianIndex(25, 4, 2),CartesianIndex(26, 4, 2),CartesianIndex(29, 6, 2),CartesianIndex(30, 6, 2),CartesianIndex(31, 6, 2)]
    @test findall(paths.indices[3]) == [CartesianIndex(11, 10, 1, 1),CartesianIndex(12, 10, 1, 1),CartesianIndex(13, 10, 1, 1),CartesianIndex(14, 10, 1, 1),CartesianIndex(11, 3, 5, 1),CartesianIndex(12, 3, 5, 1),CartesianIndex(13, 3, 5, 1),CartesianIndex(14, 3, 5, 1),CartesianIndex(15, 3, 5, 1),CartesianIndex(11, 3, 6, 1),CartesianIndex(12, 3, 6, 1),CartesianIndex(13, 3, 6, 1),CartesianIndex(14, 3, 6, 1),CartesianIndex(15, 3, 6, 1),CartesianIndex(11, 3, 7, 1),CartesianIndex(12, 3, 7, 1),CartesianIndex(13, 3, 7, 1),CartesianIndex(14, 3, 7, 1),CartesianIndex(15, 3, 7, 1)]
    @test ex[paths][3] == [fill(4.0, 4)...; fill(-5.0, 15)...]
    ex .- 3.0
    
    @test ex[paths[2]] == ex[pathLocs(1,(24:26,4),exs=2)]
    @test ex[paths[3]] == ex[pathLocs(1,(29:31,6),exs=1:2)]
    @test ex[paths[4]] == ex[pathLocs(2,(11:14,10,1),exs=1)]
    size(ex[0])
    ndims((1,3,2))
    length(paths[1].indices[1][1])
    size(paths[1].indices[1][1])
    ex
    size(paths[1].indices[1][1])
    ex[0][paths[1].indices[1]...]
    ex[paths[1]] == ex[pathLocs(0,40:46)]
    paths == (pathLocs(0,40:46), pathLocs(1,(24:26,4)), 
              pathLocs(1,(29:31,6)), pathLocs(2,(11:14,10,1)))

    # setindex using a boolean array instead of a normal one
    mostlyNull = scattered((zeros(50,1,2), zeros(34,13,2), zeros(23,11,13,2)))
    newValues = randn(35)
    mostlyNull[paths] #todo this should be a tuple not an array
    mostlyNull[paths] = newValues
    @test cat(mostlyNull[paths]..., dims=1) ≈ newValues
    # testing adding one location at a time
    nullEx = scattered((zeros(50,1,1), zeros(34,13,1), zeros(23,11,13,1)))
nullEx[0][3,1,1] = 50
nullEx[2][3:20,1,1,1] .= 25; nullEx[2][5,2:5,1,1] .= 26; nullEx[2][5,9,12:13,1] .= 2
addFrom = nonZeroPaths(nullEx, allTogetherInOne=true,wholePath=false)
Tuple{}()
addTo = addNextPath(addFrom)
@test findfirst(addTo.indices[1]!=addFrom.indices[1])==nothing
@test addTo.indices[2] == nothing
@test findfirst(addTo.indices[3])==nothing

addToNext = addNextPath(addTo, addFrom)
@test addToNext.indices[2] == nothing
@test findfirst(addToNext.indices[1]!=addFrom.indices[1])==nothing
@test findfirst(addToNext.indices[3].!=addFrom.indices[3])==CartesianIndex(4,1,1,1)
@test findall(addToNext.indices[3]) == findall(addFrom.indices[3])[1:1]
ii=2
while addTo!=addToNext && ii <=24
    global addToNext, addTo,ii
    addTo = addToNext
    addToNext = addNextPath(addTo, addFrom)
    @test findall(addToNext.indices[3]) == findall(addFrom.indices[3])[1:ii]
    ii+=1
end
@test all(addToNext.indices .== addFrom.indices)



    # probably useless junk
    # res[0]
    # y, back = pullback(x->getproperty(x, :result), ex)
    # back(y);
    # y, back = pullback(x->x.result, ex)
    # back(y);
    # y, back = pullback(x->acc(x), ex)
    # back(y);
    # y, back = pullback(x->x[p], ex)
    # res = back(y)
    # res[1]
    # getproperty(p, :indices)
    # using ChainRulesCore
    # using ChainRulesTestUtils
    # ChainRules.debug_mode() = true
    # ex = scattered((randn(50,1,1), randn(34,13,1), randn(23,11,13,1)))
    # collatingTransform.has_chain_rrule(typeof((getproperty,ex, :result)))
    # collatingTransform.Zygote.meta(Tuple{typeof(rrule), typeof((getproperty,ex,
    #                                                             :result)).parameters...})
    # rrule_test(x->x.result, ex.result, (ex, scattered((randn(50,1,1),
    #                                                   randn(34,13,1),
    #                                                   randn(23,11,13,1))))) 
    # methods(rrule)
    # y, back = pullback(x->x.result, ex);
    # res = back(y); typeof(res)
    # y, back = pullback(x->getproperty(x, :result), ex);
    # res = back(y); typeof(res)
    # y, b = rrule(getproperty, ex, :result);
    # using Revise, ChainRulesCore, Zygote
    # t = b(y);
    # typeof(t)
    # using ChainRules
    # using ChainRules, Zygote
    # struct foo
    #     bar
    # end
    # function rrule(::typeof(getproperty), F::T, x::Symbol) where T <: foo
    #     println("using the rrule")
    #     function gp_sc(Ȳ)
    #         println("this is being called")
    #         ∂F = Composite{T}(Ȳ)
    #         return NO_FIELDS, ∂F, DoesNotExist()
    #     end
    #     return getproperty(F, x), gp_sc
    # end
    # y,b = rrule(getproperty, foo(randn(10)), :bar)
    # b(y)
    # # compare with
    # y, back = pullback(x->x.bar, foo(randn(10)));
    # back(y)
    # y, back = pullback(x->x.bar, foos(randn(10)));
    # back(y)

    # wef = gradient(x->getproperty(x,:result)[1][1], ex);
    # y, back = pullback(getproperty, ex, :result);
    # res = back(y); typeof(res)
    # code_typed(back.back, (typeof(y),))
    # methods(back.back.t[1])
    # res[1][1][1]
    # y,back = pullback(x->x[1], ex)
    # pullback(getproperty, ex, :scattered)
    # res = back(y)
    # typeof(res)
    # res[1][1]
    # res[1]
    # p.indices
    # y, back = pullback(x->x[1], ex);
    # res = back(y)
    # res[1][1]
    # y, b = collatingTransform.rrule(getindex, ex, 1)
    # b(y)
    # # definitely needs a test

    # typeof(y)
    # back(y)
    # yy, bback = pullback(x->x[:,4:9,5,:], ex.result[3])
    # typeof(yy)
    # res = bback(yy);
    # size(res[1])
end

