using Revise
using CUDA, Zygote, Flux, FFTW
using collatingTransform
using Plots
using Flux: mse, crossentropy, @epochs
using cartoonLike, LinearAlgebra

# fitting a single collating layer
fitThisOne = collatingLayer((50,50, 1, 1), 1=>17, σ=abs,
               init=iden_perturbed_gaussian, scale=2) |> gpu
toThisOne = collatingLayer((50,50, 1, 1), 1=>17, σ=abs,
               init=iden_perturbed_gaussian, scale=2) |> gpu

minimalLoss(x) = mse(fitThisOne(x), toThisOne(x))
randExamples(k) = [cu(10 .* randn(50,50,1,1)) for i=1:k]
[minimalLoss(x) for x in randExamples(30)]
nEpochs = 30; nSamples = 30
fittingCurveOne = zeros(nEpochs);
paramCurveOne = zeros(nEpochs);
evalcb() = @show(sum([minimalLoss(x) for x in randExamples(11)]))
evalcb()

for epoch=1:nEpochs
    if fittingCurveOne[end]>0
        append!(fittingCurveOne, zeros(nEpochs))
        expandRecord!(recordDum, nEpochs)
    end
    Flux.train!((x)->minimalLoss(x), 
                params(fitThisOne), 
                randExamples(nSamples), ADAM(), cb = evalcb)
    putIn = length(fittingCurveOne)-nEpochs
    fittingCurveOne[putIn + epoch] = sum([minimalLoss(x) for x in randExamples(11)])
    paramCurveOne[putIn + epoch] = norm(params(fitThisOne) .- params(toThisOne))
    println("$(putIn+epoch), $(fittingCurveOne[putIn + epoch])")
    #@time addCurrent!(recordDum,fitThis,putIn+epoch)
end
plot(log.(fittingCurveOne))
plot(paramCurveOne)

# two collating layers
ch = [1,11,11]
n = 1
firstPool = RationPool((3//2,3//2))
secondPool = RationPool((5//4,5//4))
startSize = (50,50)
firstSize = poolSize(firstPool,startSize)
secondSize = poolSize(secondPool,firstSize)
nShears = 17
nScales = 2
nonlin = abs
centerr =floor.(Int,secondSize ./ 2)
offset = ceil.(Int, 5*secondSize[1]./startSize[1])
centerSize = [(x.-offset):x+offset for x in centerr]

fitThis = Chain(collatingLayer((startSize..., 1, n), 1=>ch[1], σ=nonlin,
                               scale=nScales),
                firstPool, 
                collatingLayer((firstSize..., ch[1]*nShears, n),
                               ch[1]*nShears=>ch[2], σ=nonlin,
                               scale=nScales)) |> gpu
size(fitThis(randExamples(1)[1]))
toThis = Chain(collatingLayer((startSize..., 1, n), 1=>ch[1], σ=nonlin,
                               scale=nScales),
                firstPool, 
                collatingLayer((firstSize..., ch[1]*nShears, n),
                               ch[1]*nShears=>ch[2], σ=nonlin,
                               scale=nScales)) |> gpu

minimalLoss(x) = mse(fitThis(x), toThis(x))
randExamples(k) = [cu(10 .* randn(50,50,1,1)) for i=1:k]
k = 11
function semiRandExamples(k)
    isCurve = rand(k) .> .5
    curveInds = rand(1:11, k)
    [isCurve[ii] ? curveExamples[:,:,:,curveInds[ii]:curveInds[ii]] : 
     cu(10 .* randn(50,50,1,1)) for ii=1:k]
end
randExamples(k) = [curveExamples[:,:,:, i:i] for i in
                   rand(1:(nPoints*2+1),k)]

[minimalLoss(x) for x in semiRandExamples(30)]
nEpochs = 10; nSamples = 30
fittingCurveDum = zeros(nEpochs);
paramCurveDum = zeros(nEpochs);
evalcb() = @show(sum([minimalLoss(x) for x in randExamples(11)]))

for epoch=1:nEpochs
    if fittingCurveDum[end]>0
        append!(fittingCurveDum, zeros(nEpochs))
    end
    if paramCurveDum[end]>0
        append!(paramCurveDum, zeros(nEpochs))
    end
    Flux.train!((x)->minimalLoss(x), 
                params(fitThis), 
                semiRandExamples(nSamples), ADAM(.0000001))
    putIn = length(fittingCurveDum)-nEpochs
    fittingCurveDum[putIn + epoch] = sum([minimalLoss(x) for x in semiRandExamples(11)])
    paramCurveDum[putIn + epoch] = norm(params(fitThis) .- params(toThis))
    println("$(putIn+epoch), $(fittingCurveDum[putIn + epoch])")
    #@time addCurrent!(recordDum,fitThis,putIn+epoch)
end

gr()

plot(plot(log.(fittingCurveDum), title="log fit curve"),
     plot(log.(paramCurveDum), title="param curve"), 
     layout=2)

params(toThis).order[5]
length(params(fitThis).order)

plot(scatter(params(fitThis).order[1][:],params(toThis).order[1][:],
             xlims = (-5,5), ylims = (-5,5), legend=false, title="first coefs"),
     scatter(params(fitThis).order[2], params(toThis).order[2], xlims = (-5,5),
             ylims = (-5,5), legend=false, title="first bias"),
     heatmap(cpu((params(toThis).order[3] - params(fitThis).order[3])[1,1,1,:,:]), title="toThis - fitThis second"), 
     heatmap(cpu(params(fitThis).order[3][1,1,1,:,:]), title="fitThis second"),
     heatmap(cpu(params(toThis).order[3][1,1,1,:,:]), title="toThis second"),
     plot([params(fitThis).order[4] params(toThis).order[4]], 
          labels=["fitThis" "toThis"], label="second bias"),
     layout=6)
length(params(fitThis).order)
size(params(fitThis).order[2])

# three collating layers
ch = [1,11,11]
n = 1
firstPool = RationPool((3//2,3//2))
secondPool = RationPool((5//4,5//4))
startSize = (50,50)
firstSize = poolSize(firstPool,startSize)
secondSize = poolSize(secondPool,firstSize)
nShears = 17
nScales = 2
nonlin = abs
centerr =floor.(Int,secondSize ./ 2)
offset = ceil.(Int, 5*secondSize[1]./startSize[1])
centerSize = [(x.-offset):x+offset for x in centerr]

fitThis = Chain(collatingLayer((startSize..., 1, n), 1=>ch[1], σ=nonlin,
                               scale=nScales),
                firstPool, 
                collatingLayer((firstSize..., ch[1]*nShears, n),
                               ch[1]*nShears=>ch[2], σ=nonlin,
                               scale=nScales),
                secondPool, 
                collatingLayer((secondSize..., ch[2]*nShears, n),
                               ch[2]*nShears=>ch[3], σ=nonlin,
                               scale=nScales),
                (x) -> x[centerSize..., :, :], 
                (x) -> reshape(x, (prod(length.(centerSize))*nShears*ch[3], :))
                ) |> gpu
fitThis(randExamples(1)[1])
toThis = Chain(collatingLayer((startSize..., 1, n), 1=>ch[1], σ=nonlin,
                              init=iden_perturbed_gaussian, scale=nScales),
                firstPool, 
                collatingLayer((firstSize..., ch[1]*nShears, n),
                               ch[1]*nShears=>ch[2], σ=nonlin,
                               init=uniform_perturbed_gaussian, scale=nScales),
                secondPool, 
                collatingLayer((secondSize..., ch[2]*nShears, n),
                               ch[2]*nShears=>ch[3], σ=nonlin,
                               init=uniform_perturbed_gaussian, scale=nScales),
                (x) -> x[centerSize..., :, :], 
                (x) -> reshape(x, (prod(length.(centerSize))*nShears*ch[3], :))
                ) |> gpu

minimalLoss(x) = mse(fitThis(x), toThis(x))
randExamples(k) = [cu(10 .* randn(50,50,1,1)) for i=1:k]
k = 11
function semiRandExamples(k)
    isCurve = rand(k) .> .5
    curveInds = rand(1:11, k)
    [isCurve[ii] ? curveExamples[:,:,:,curveInds[ii]:curveInds[ii]] : 
     cu(10 .* randn(50,50,1,1)) for ii=1:k]
end

[minimalLoss(x) for x in semiRandExamples(30)]
[minimalLoss(x) for x in randExamples(30)]
nEpochs = 40; nSamples = 30
fittingCurveDum = zeros(nEpochs);
paramCurveDum = zeros(nEpochs);
evalcb() = @show(sum([minimalLoss(x) for x in randExamples(11)]))

for epoch=1:nEpochs
    if fittingCurveDum[end]>0
        append!(fittingCurveDum, zeros(nEpochs))
    end
    if paramCurveDum[end]>0
        append!(paramCurveDum, zeros(nEpochs))
    end
    Flux.train!((x)->minimalLoss(x), 
                params(fitThis), 
                semiRandExamples(nSamples), ADAM(.001))
    putIn = length(fittingCurveDum)-nEpochs
    fittingCurveDum[putIn + epoch] = sum([minimalLoss(x) for x in semiRandExamples(11)])
    paramCurveDum[putIn + epoch] = norm(params(fitThis) .- params(toThis))
    println("$(putIn+epoch), $(fittingCurveDum[putIn + epoch])")
    #@time addCurrent!(recordDum,fitThis,putIn+epoch)
end

plot(plot(log.(fittingCurveDum), title="log fit curve"),plot(paramCurveDum,
                                                             title="param curve"), layout=2)

plot(scatter(params(fitThis).order[1][1,1,1,1,:],params(toThis).order[1][1,1,1,1,:],
             xlims = (-5,5), ylims = (-5,5), legend=false, title="first coefs"),
     plot([params(fitThis).order[2] params(toThis).order[2]], 
          labels=["fitThis" "toThis"],title="first bias"), 
     heatmap(cpu((params(toThis).order[3] - params(fitThis).order[3])[1,1,1,:,:]), title="toThis - fitThis second"), 
     heatmap(cpu(params(fitThis).order[3][1,1,1,:,:]), title="fitThis second"),
     plot([params(fitThis).order[4] params(toThis).order[4]], 
          labels=["fitThis" "toThis"], label="second bias"),
     heatmap(cpu((params(toThis).order[5] - params(fitThis).order[5])[1,1,1,:,:]), title="toThis - fitThis third"), 
     heatmap(cpu(params(fitThis).order[5][1,1,1,:,:]), title="fitThis third"),
     plot([params(fitThis).order[6] params(toThis).order[6]], 
          labels=["fitThis" "toThis"], label="third bias"),
     layout=8)

oldFitThis = fitThis
oldToThis = toThis
oldfittingCurveDum = fittingCurveDum
oldparamCurveDum = paramCurveDum


using Flux
# The convolution should be over the 4th dimension, not the 3rd
typeof(params(fitThis).order[3])
[size(x) for x in params(fitThis).order]
length(fitThis.layers[1].layers)
exCol = collatingLayer((startSize..., 1, n), 1=>1, σ=nonlin,
                       init=iden_perturbed_gaussian, scale=nScales)
size(exCol(cu(randn(50,50,1,1))))
exCol = collatingLayer((startSize..., 17, n), 17=>1, σ=nonlin,
                       init=iden_perturbed_gaussian, scale=nScales)
size(exCol.layers[1](cu(randn(50,50,1,1))))
oneExample = fitThis.layers[1].layers[1](cu(randn(50,50,1,1))); size(oneExample)
oneExample = randn(50,50,17,1); size(oneExample)
size(gpu(exCol(oneExample)))


x = oneExample = randn(50,50,17,3,1); size(oneExample)

struct convReduce{D,N}
    cs::NTuple{N,<:Conv}
end
"""
fully connected layer across the non-convolution dimensions, excluding the
  very last. D gives the number of convolution dimensions, while N gives the
  input size (the dimension immediately after the conv dimension).
Example: for a 2D convolution, with a signal of `size (50,50,17,3,100)`, and `ch`
    is `3=>2`, N should be 17.
"""
function convReduce(ch, N, D, nonLin)
    cs = ([Conv((fill(1,max(D,2))...,), ch, nonLin) for i=1:N]...,)
    return convReduce{D, N}(cs)
end
function (c::convReduce{D,N})(x) where {D,N}
    cat([applySingle(cs[ii], x, ii) for ii= 1:N]..., dims= D+1)
end
function applySingle(c, x::AbstractArray{<:Any,5}, ii)
    ax = axes(x)
    clipped = x[ax[1:2]..., ii, ax[4:end]...]
    c(clipped)
end
function applySingle(c, x::AbstractArray{<:Any,4}, ii)
    ax = axes(x)
    clipped = x[ax[1], ii:ii, ax[3:end]...] # have to add a dead variable because
    # 1D convolution isn't supported
    size(clipped)
    res = c(clipped)
    reshape(res, (size(res,1), size(res)[3:end]...))
end
c.convs[1]
wef = convReduce(3=>2,17,2,abs)
fieldnames(typeof(wef.convs[1]))
convs = convReduce(17, 3=>2) |> gpu

inChan = 3
outChan = 2
nFrameEl = 17
nDims = 2
N = 2
nonLin = identity

cs = ([Conv((fill(1,N)...,), inChan=>outChan, nonLin) for i=1:nFrameEl]...,) |> gpu
size(cat([applySingle(cs[ii], x,N) for ii= 1:length(cs)]..., dims= N+1))

x = gpu(randn(50,17,3,1)); size(oneExample)
N = 1
cs = ([Conv((fill(1,2)...,), inChan=>outChan, nonLin) for i=1:nFrameEl]...,) |> gpu
return cat([applySingle(cs[ii], x) for ii= 1:length(cs)]..., dims= N+1)



c(x[ax[1:N]..., 1, ax[N+2:end]...])
c = Conv((1,1), inChan=>outChan, nonLin) |> gpu
ax = axes(oneExample)
size(view(oneExample, ax[1:2]..., 1, ax[4:end]...))
viewed = view(oneExample, ax[1:2]..., 1, ax[4:end]...); size(viewed)
viewed = reshape(oneExample[ax[1:2]..., 1, ax[4:end]...], (50,50,3,1));
viewed = oneExample[ax[1:2]..., 1, ax[4:end]...]; (size(viewed),typeof(viewed))
@test size(c.weight) == (1,1,inChan,outChan)
@test size(cs[1].weight) == (1,1,inChan,outChan)
size(c(viewed))
@time c(view(oneExample, ax[1:2]..., 1, ax[4:end]...))
c
@time size(c(oneExample[ax[1:2]..., 1, ax[4:end]...]))
size(c(oneExample))
fieldnames(typeof(c))
size(c.weight)
size(c.bias)
# normal conv layer
c = Conv((3,3), 1=>13)
size(c.weight)
size(c(oneExample[:,:,:,:]))


# collating layer and a dense layer
fitThis = Chain(collatingLayer((50,50, 1, 1), 1=>17, σ=abs,
                               init=iden_perturbed_gaussian, scale=2),
                (x) -> reshape(x, (50*50*17*17, :)),
                Dense(50*50*17*17,11)) |> gpu
@time fitThis(cu(randn(50,50,1,1)))
toThis = Chain(collatingLayer((50,50, 1, 1), 1=>17, σ=abs, scale=2),
               (x) -> reshape(x, (50*50*17*17, :)),
               Dense(50*50*17*17,11)) |> gpu
@time toThis(cu(randn(50,50,1,1)))
params(fitThis) .-params(toThis)
plot([fitThis.layers[1].layers[2].weight[1,1,1,1,:] toThis.layers[1].layers[2].weight[1,1,1,1,:]], labels=["fitThis" "toThis"])
#plot(heatmap(fitThis.layers[3].W), heatmap(toThis.layers[3].W), layout=2)
minimalLoss(x) = mse(fitThis(x), toThis(x))
randExamples(k) = [cu(10 .* randn(50,50,1,1)) for i=1:k]
[minimalLoss(x) for x in randExamples(30)]
nEpochs = 10; nSamples = 50
fittingCurveDum = zeros(nEpochs);
paramCurveDum = zeros(nEpochs);
paramFirstDum = zeros(nEpochs);
evalcb() = @show(sum([minimalLoss(x) for x in randExamples(11)]))
evalcb()
norm(params(fitThis) .- params(toThis))

for epoch=1:nEpochs
    if fittingCurveDum[end]>0
        append!(fittingCurveDum, zeros(nEpochs))
    end
    if paramCurveDum[end]>0
        append!(paramCurveDum, zeros(nEpochs))
    end
    if paramFirstDum[end]>0
        append!(paramFirstDum, zeros(nEpochs))
    end
    Flux.train!((x)->minimalLoss(x), 
                params(fitThis), 
                randExamples(nSamples), ADAM())
    putIn = length(fittingCurveDum)-nEpochs
    fittingCurveDum[putIn + epoch] = sum([minimalLoss(x) for x in randExamples(11)])
    putIn = length(paramCurveDum)-nEpochs
    paramCurveDum[putIn + epoch] = norm(params(fitThis) .- params(toThis))
    putIn = length(paramFirstDum)-nEpochs
    paramFirstDum[putIn + epoch] = norm(fitThis.layers[1].layers[2].weight[1,1,1,1,:] - toThis.layers[1].layers[2].weight[1,1,1,1,:])
    println("$(putIn+epoch), $(fittingCurveDum[putIn + epoch])")
    #@time addCurrent!(recordDum,fitThis,putIn+epoch)
end
norm(params(fitThis) .-params(toThis))
plot([fitThis.layers[1].layers[2].weight[1,1,1,1,:] toThis.layers[1].layers[2].weight[1,1,1,1,:]], labels=["fitThis" "toThis"])

plot(log.(fittingCurveDum[fittingCurveDum .>0]))
plot(paramFirstDum)
plot(paramCurveDum)
erer




# multiple layers
nPoints = 5
curveExamples = distinguishing_curvature(nPoints,cart = cartoonLikeImage{Float32}(xSize=50,ySize=50))
plot(heatmap(curveExamples[:,:,3]), heatmap(curveExamples[:,:,11]), layout=(1,2))
curveExamples = Float32.(reshape(curveExamples, (size(curveExamples)[1:2]..., 1,
                                                 size(curveExamples, 3)))) |> gpu
κ = cu(Float32.(I(11)).* ones(Float32, 11, 11))
size(curveExamples)
ch = [11,11,11]
n = 1
firstPool = RationPool((3//2,3//2))
secondPool = RationPool((5//4,5//4))

startSize = size(curveExamples)[1:2]
firstSize = poolSize(firstPool,startSize)
secondSize = poolSize(secondPool,firstSize)

nShears = 17
nScales = 2

nonlin = abs

# only selecting the coefficients in the neighborhood of the curve
centerr =floor.(Int,secondSize ./ 2)
offset = ceil.(Int, 5*secondSize[1]./startSize[1])
centerSize = [(x.-offset):x+offset for x in centerr]

fitThis = Chain(collatingLayer((startSize..., 1, n), 1=>ch[1], σ=nonlin,
                             init=iden_perturbed_gaussian, scale=nScales),
              firstPool,
              collatingLayer((firstSize..., ch[1]*nShears, n), ch[1]*nShears=>ch[2],
                             σ=nonlin, init=uniform_perturbed_gaussian,
                             scale=nScales),
              secondPool,
              collatingLayer((secondSize..., ch[2]*nShears, n), ch[2]*nShears=>ch[3],
                             σ=nonlin, init=uniform_perturbed_gaussian,
                             scale=nScales),
              (x) -> x[centerSize..., :, :],
              (x) -> reshape(x, (prod(length.(centerSize))*nShears*ch[3], :)),
              Dense(prod(length.(centerSize))*nShears*ch[3], 11, nonlin), 
              softmax) |> gpu
fitUsing = Chain(collatingLayer((startSize..., 1, n), 1=>ch[1], σ=nonlin,
                             init=iden_perturbed_gaussian, scale=nScales),
              firstPool,
              collatingLayer((firstSize..., ch[1]*nShears, n), ch[1]*nShears=>ch[2],
                             σ=nonlin, init=uniform_perturbed_gaussian,
                             scale=nScales),
              secondPool,
              collatingLayer((secondSize..., ch[2]*nShears, n), ch[2]*nShears=>ch[3],
                             σ=nonlin, init=uniform_perturbed_gaussian,
                             scale=nScales),
              (x) -> x[centerSize..., :, :],
              (x) -> reshape(x, (prod(length.(centerSize))*nShears*ch[3], :)),
              Dense(prod(length.(centerSize))*nShears*ch[3], 11, nonlin), 
              softmax) |> gpu
dummyLoss(x) = crossentropy(fitThis(x), fitUsing(x))

@time dummyLoss(curveExamples[:,:,:,1:1])
@time ∇ = gradient(()->dummyLoss(curveExamples[:, :, :, 1:1]),
                   params(fitThis))
randExamples(k) = [(curveExamples[:,:,:, i:i], κ[:,i]) for i in rand(1:(nPoints*2+1))]
nEpochs = 600; nSamples = 30
fittingCurveDum = zeros(nEpochs);
recordDum = buildRecord(fitThis, nEpochs);
addCurrent!(recordDum,fitThis,0)
evalcb() = @show(sum([dummyLoss(curveExamples[:,:,:,i:i]) for i=1:11]))

for epoch=1:nEpochs
    if fittingCurveDum[end]>0
        append!(fittingCurveDum, zeros(nEpochs))
        expandRecord!(recordDum, nEpochs)
    end
    Flux.train!((x,y)->dummyLoss(x), 
                params(fitThis), 
                randExamples(nSamples), ADAM())
    putIn = length(fittingCurveDum)-nEpochs
    fittingCurveDum[putIn + epoch] = sum([dummyLoss(curveExamples[:,:,:,i:i]) 
                                       for i=1:11])
    println("$(putIn+epoch), $(fittingCurveDum[putIn + epoch])")
    addCurrent!(recordDum,fitThis,putIn+epoch)
end
