using Pkg;
Pkg.activate(joinpath(@__DIR__, "..")); Pkg.instantiate()
Pkg.activate(); Pkg.instantiate()

pushfirst!(LOAD_PATH, joinpath(@__DIR__, ".."))

using Documenter, collatingTransform

makedocs(;
    modules=[collatingTransform],
    format=Documenter.HTML(),
    pages=[
        "Home" => "index.md",
    ],
    repo="https://github.com/dsweber2/ScatteringTransform.jl/blob/{commit}{path}#L{line}",
    sitename="collatingTransform",
    authors="dsweber2",
    assets=String[],
)

deploydocs(;
    repo="github.com/dsweber2/collatingTransform.jl.git",
)
